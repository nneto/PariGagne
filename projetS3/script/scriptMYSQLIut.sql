DROP DATABASE pariSgagne;
DROP DATABASE if EXISTS parisgagne;
CREATE DATABASE IF NOT EXISTS parisgagne;


-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Mar 08 Novembre 2016 à 03:28
-- Version du serveur :  5.7.9
-- Version de PHP :  5.6.16

SET FOREIGN_KEY_CHECKS = 0;
/*SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";
*/
USE parisgagne;
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `parisgagne`
--

-- --------------------------------------------------------


DROP TABLE IF EXISTS `token`;

CREATE TABLE IF not EXISTS `token` (
  `token` VARCHAR (20) PRIMARY KEY ,
  `creation` DATETIME ,
  `expiration`  INT UNSIGNED

)ENGINE=InnoDB DEFAULT CHARSET=utf8;;
--
-- Structure de la table `compte`
--

DROP TABLE IF EXISTS `compte`;
CREATE TABLE IF NOT EXISTS `compte` (
  `idcompte` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `motDePasse` text NOT NULL,
  `solde` double NOT NULL DEFAULT '100',
  `pseudo` varchar(30) NOT NULL,
  `avatar` varchar(255) NOT NULL DEFAULT 'avatar.png',
  `admin` BOOL NOT NULL DEFAULT '0',
  `idPers` int(11) NOT NULL,
  PRIMARY KEY (`idcompte`),
  UNIQUE KEY `email` (`email`,`pseudo`),
  UNIQUE KEY `compte_pseudo_uindex` (`pseudo`),
  check (solde>=0),
  KEY `FK_compte_idPers` (`idPers`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `compte_statcompte`
--

DROP TABLE IF EXISTS `compte_statcompte`;
CREATE TABLE IF NOT EXISTS `compte_statcompte` (
  `idStat` int(11) NOT NULL,
  `idcompte` int(11) NOT NULL,
  PRIMARY KEY (`idStat`,`idcompte`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `equipe`
--

DROP TABLE IF EXISTS `equipe`;
CREATE TABLE IF NOT EXISTS `equipe` (
  `idteam` int(11) NOT NULL AUTO_INCREMENT,
  `nomTeam` varchar(100) NOT NULL,
  `logo` varchar(255) DEFAULT 'logodefault.png',

  PRIMARY KEY (`idteam`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `equipe`
--


-- --------------------------------------------------------

--
-- Structure de la table `equipe_statequipe`
--

DROP TABLE IF EXISTS `equipe_statequipe`;
CREATE TABLE IF NOT EXISTS `equipe_statequipe` (
  `idStat` int(11) NOT NULL,
  `idteam` int(11) NOT NULL,
  PRIMARY KEY (`idStat`,`idteam`),
  KEY `FK_equipe_statEquipe_idteam` (`idteam`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `estamis`
--

DROP TABLE IF EXISTS `estamis`;
CREATE TABLE IF NOT EXISTS `estamis` (
  `idPers` int(11) NOT NULL,
  `idPers_Personne` int(11) NOT NULL,
  PRIMARY KEY (`idPers`,`idPers_Personne`),
  KEY `FK_estAmis_idPers_Personne` (`idPers_Personne`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `matchs`
--


DROP TABLE IF EXISTS `matchs`;
CREATE TABLE IF NOT EXISTS `matchs` (
  `idmatch` int(11) NOT NULL AUTO_INCREMENT,
  `dateMatch` date NOT NULL,
  `heureMatch` time NOT NULL,
  `nomMatch` varchar(255) NOT NULL,
  `gagnant` int(11) DEFAULT NULL,
  `idSport` int(11) DEFAULT NULL,
  PRIMARY KEY (`idmatch`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `parie`
--

DROP TABLE IF EXISTS `parie`;
CREATE TABLE IF NOT EXISTS `parie` (
  `idParie` int(11) NOT NULL,
  `mise` double DEFAULT NULL,
  `dateParie` date DEFAULT NULL,
  `heureParie` time DEFAULT NULL,
  `gagne` tinyint(1) DEFAULT NULL,
  `idCompte` int(11) NOT NULL,
  `idmatch` int(11) NOT NULL,
  `idTeam` int(11) NOT NULL,
  PRIMARY KEY (`idCompte`,`idmatch`),
  CHECK (mise >0),
  KEY `FK_parie_idmatch` (`idmatch`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `participe`
--

DROP TABLE IF EXISTS `participe`;
CREATE TABLE IF NOT EXISTS `participe` (
  `cote` double DEFAULT '1',
  `idteam` int(11) NOT NULL,
  `idmatch` int(11) NOT NULL,
  `score` int(11) DEFAULT 0,
  PRIMARY KEY (`idteam`,`idmatch`),
  KEY `FK_participe_idmatch` (`idmatch`),
  CHECK (cote >=1)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `personne`
--

DROP TABLE IF EXISTS `personne`;
CREATE TABLE IF NOT EXISTS `personne` (
  `idPers` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(100) NOT NULL,
  `prenom` varchar(100) NOT NULL,
  `dateDeNaissance` date NOT NULL,
  `idcompte` INT(11),
  PRIMARY KEY (`idPers`)
  -- CHECK (Floor(DATEDIFF(CURDATE(),dateDeNaissance)/365)>=18)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `statistiquecompte`
--

DROP TABLE IF EXISTS `statistiquecompte`;
CREATE TABLE IF NOT EXISTS `statistiquecompte` (
  `idStat` int(11) NOT NULL AUTO_INCREMENT,
  `gainTotal` double DEFAULT '0',
  `gainMoyen` double DEFAULT '0',
  `miseJouer` double DEFAULT '0',
  `miseMoyen` double DEFAULT '0',
  `dateStat` date DEFAULT NULL,
  PRIMARY KEY (`idStat`),
  CHECK (gainTotal>=0),
  CHECK (gainMoyen>=0),
  CHECK (miseJouer>=0),
  CHECK (miseMoyen>=0)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Structure de la table `stat_equipe`
--

DROP TABLE IF EXISTS `stat_equipe`;
CREATE TABLE IF NOT EXISTS `stat_equipe` (
  `idStat` int(11) NOT NULL AUTO_INCREMENT,
  `coteMoyenne` double DEFAULT NULL,
  `dateStat` date NOT NULL,
  `serieDeVictoire` int(11) DEFAULT '0',
  `MatchGagneTotal` int(11) DEFAULT '0',
  PRIMARY KEY (`idStat`),
  CHECK (coteMoyenne>=1),
  CHECK (serieDeVictoire>=0),
  CHECK (MatchGagneTotal>=0)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
--
-- Structure de la table `sport`
--
DROP TABLE IF EXISTS `sport`;
CREATE TABLE IF NOT EXISTS `sport` (
  `idSport` int(11) NOT NULL AUTO_INCREMENT,
  `nomSport` VARCHAR(255) NOT NULL UNIQUE ,
  PRIMARY KEY (`idSport`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour les tables exportées
--
--
-- Contraintes pour la table `compte`
--
 
ALTER TABLE `compte`
  ADD CONSTRAINT `FK_compte_idPers` FOREIGN KEY (`idPers`) REFERENCES `personne` (`idPers`)  ON DELETE CASCADE;

--
-- Contraintes pour la table `compte_statcompte`
--
ALTER TABLE `compte_statcompte`
  ADD CONSTRAINT `FK_compte_statCompte_idStat` FOREIGN KEY (`idStat`) REFERENCES `statistiquecompte` (`idStat`)  ON DELETE CASCADE,
  ADD CONSTRAINT `FK_compte_statCompte_idcompte` FOREIGN KEY (`idcompte`) REFERENCES `compte` (`idcompte`)  ON DELETE CASCADE;
--
-- Contraintes pour la table `match`
--


--
-- Contraintes pour la table `equipe_statequipe`
--
ALTER TABLE `equipe_statequipe`
  ADD CONSTRAINT `FK_equipe_statEquipe_idStat` FOREIGN KEY (`idStat`) REFERENCES `stat_equipe` (`idStat`)  ON DELETE CASCADE,
  ADD CONSTRAINT `FK_equipe_statEquipe_idteam` FOREIGN KEY (`idteam`) REFERENCES `equipe` (`idteam`)  ON DELETE CASCADE;

--
-- Contraintes pour la table `estamis`
--

ALTER TABLE `estamis`
  ADD CONSTRAINT `FK_estAmis_idPers` FOREIGN KEY (`idPers`) REFERENCES `personne` (`idPers`)  ON DELETE CASCADE,
  ADD CONSTRAINT `FK_estAmis_idPers_Personne` FOREIGN KEY (`idPers_Personne`) REFERENCES `personne` (`idPers`)  ON DELETE CASCADE;

--
-- Contraintes pour la table `matchs`
--
Alter TABLE `matchs`
    ADD CONSTRAINT `FK_matchs_gagnant` FOREIGN KEY (`gagnant`) REFERENCES `equipe`(`idteam`)  ON DELETE CASCADE,
      ADD CONSTRAINT `FK_equipe_idSport` FOREIGN KEY (`idSport`) REFERENCES `sport`(`idSport`)  ON DELETE CASCADE;
--
-- Contraintes pour la table `parie`
--
ALTER TABLE `parie`
  ADD CONSTRAINT `FK_parie_idCompte` FOREIGN KEY (`idCompte`) REFERENCES `compte` (`idCompte`)  ON DELETE CASCADE,
  ADD CONSTRAINT `FK_parie_idmatch` FOREIGN KEY (`idmatch`) REFERENCES `matchs` (`idmatch`)  ON DELETE CASCADE,
  ADD CONSTRAINT `FK_parie_idTeam` FOREIGN KEY (`idTeam`) REFERENCES `equipe` (`idTeam`)  ON DELETE CASCADE;


--
-- Contraintes pour la table `participe`
--
ALTER TABLE `participe`
  ADD CONSTRAINT `FK_participe_idmatch` FOREIGN KEY (`idmatch`) REFERENCES `matchs` (`idmatch`)  ON DELETE CASCADE,
  ADD CONSTRAINT `FK_participe_idteam` FOREIGN KEY (`idteam`) REFERENCES `equipe` (`idteam`)  ON DELETE CASCADE;


--
-- Contraintes pour la table `personne`
--


ALTER TABLE `personne`
    ADD CONSTRAINT `FK_Personne_idcompte` FOREIGN KEY (`idcompte`) REFERENCES `compte` (`idcompte`)  ON DELETE CASCADE;


--
-- Contraintes pour la table `stat_equipe`
--

--
-- Contraintes pour la table `stat_compte`
--

/*********************************************************
           Retourne l'id max de la table parier +1
**********************************************************/
DROP FUNCTION IF EXISTS idPlusParier;
CREATE FUNCTION idPlusParier()
  RETURNS INT
  BEGIN
    Declare id INT;
    select idParie INTO id FROM parie ORDER by idParie DESC LIMIT 1 ;
    IF id IS NULL THEN
      SET id=0;
    END IF;
    SET id=id+1;
    Return id;
  END;
SET FOREIGN_KEY_CHECKS = 1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*function idPlusParier*/
DELIMITER ;



