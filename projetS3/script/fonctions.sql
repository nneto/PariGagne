/*function idPlusParier*/

DROP FUNCTION IF EXISTS idPlusParier;

/*********************************************************
           Retourne l'id max de la table parier +1
**********************************************************/
DROP FUNCTION IF EXISTS idPlusParier;
CREATE FUNCTION idPlusParier() RETURNS INT
  BEGIN
    Declare id INT;
    select idParie INTO id FROM parie ORDER by idParie DESC LIMIT 1 ;
    IF id IS NULL THEN
      SET id=0;
    END IF;
    SET id=id+1;
    Return id;
  END;
