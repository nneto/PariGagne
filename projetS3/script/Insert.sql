

SET FOREIGN_KEY_CHECKS = 0;
USE parisgagne;

delete FROM compte where idcompte>=0;
DELETE from compte_statcompte WHERE idcompte>=0;
DELETE from equipe where idteam>=0;
DELETE from equipe_statequipe WHERE idteam>=0;
DELETE from estamis WHERE idPers>=0;
DELETE from matchs where idmatch>=0;
DELETE from parie WHERE idCompte>=0;
DELETE FROM participe WHERE idteam>=0;
DELETE from personne WHERE idPers>=0;
DELETE from sport WHERE idSport>=0;
DELETE from stat_equipe WHERE idStat>=0;
DELETE from statistiquecompte WHERE idStat>=0;
INSERT into personne (idPers,nom, prenom,dateDeNaissance) VALUES
  (1,'neto','nicolas','1997-10-02'),
  (2,'yeles','nacer','1996-01-16'),
  (3,'touati','sonia','2016-04-26'),
  (4,'michel','jean-michel','1992-06-30'),
  (5,'micheline','micheline','1978-11-01');

INSERT INTO compte(idcompte,email, motDePasse, solde, pseudo, avatar, admin,idPers) VALUES
  (1,'nneto@iut.univ-paris8.fr','jnHIHp7wibPLY',DEFAULT ,'nneto',DEFAULT ,DEFAULT,1 ),
  (2,'neyeles@iut.univ-paris8.fr','jneSU/QVvKChY',DEFAULT ,'nyeles',DEFAULT ,DEFAULT,2 ),
  (3,'stouati@iut.univ-paris8.fr','jnFuAmXBRXdtA',DEFAULT ,'stouati',DEFAULT ,DEFAULT,3 ),
  (4,'jmichel@iut.univ-paris8.fr','jn8uVph1Lt7g6',DEFAULT ,'jmmichel',DEFAULT ,DEFAULT,4 ),
  (5,'mmicheline@iut.univ-paris8.fr','jnrsxdUhXx8kE',DEFAULT ,'mmicheline',DEFAULT ,1,5 );

UPDATE personne SET  idcompte =1 WHERE idPers=1;
UPDATE personne SET  idcompte =2 WHERE idPers=2;
UPDATE personne SET  idcompte =3 WHERE idPers=3;
UPDATE personne SET  idcompte =4 WHERE idPers=4;
UPDATE personne SET  idcompte =5 WHERE idPers=5;

INSERT INTO sport (idSport,nomSport) VALUES
(1,'football'),
(2,'basketball'),
(3,'tennis'),
(4,'boxe')

;
INSERT INTO equipe (idteam,logo, nomTeam) VALUES
  (1000,0,'NUL'),
  (1,DEFAULT ,'ANGER SC0' ),
  (2,DEFAULT ,'SC BASTIA' ),
  (3,DEFAULT ,'GIRONDINS DE BORDEAUX' ),
  (4,DEFAULT ,'SM CAEN' ),
  (5,DEFAULT ,'DIJON FCO' ),
  (6,DEFAULT ,'EA GUINGAMP' ),
  (7,DEFAULT ,'LILLE OSC' ),
  (8,DEFAULT ,'FC LORIENT' ),
  (9,DEFAULT ,'OLYMPIQUE LYONNAIS' ),
  (10,DEFAULT ,'OLYMPIQUE DE MARSEILLE' ),
  (11,DEFAULT ,'FC METZ' ),
  (12,DEFAULT ,'AS MONACO' ),
  (13,DEFAULT ,'MONTPELLIER HSC' ),
  (14,DEFAULT ,'AS NANCY-LORRAINE' ),
  (15,DEFAULT ,'FC NANTES' ),
  (16,DEFAULT ,'OGC NICE' ),
  (17,DEFAULT ,'PARIS SAINT-GERMAIN'),
  (18,DEFAULT ,'STADE RENNAIS' ),
  (19,DEFAULT ,'AS SAINT-ETIENNE'),
  (20,DEFAULT ,'TOULOUSE FC' ),
  (21,DEFAULT ,'Châlons-Reims'),
  (22,DEFAULT ,'Le Mans')
    ;
UPDATE equipe set idteam=0 where nomTeam='NUL';
INSERT INTO matchs (idmatch,dateMatch, heureMatch, nomMatch, gagnant,idSport) VALUES
  (1,'2016-11-25','20:45:00','Ligue 1 14 ème journée Toulouse  vs Renne ',18,1 ),
  (2,'2016-11-26','17:00:00','Ligue 1 14 ème journée Monaco  vs OM ',12 ,1),
  (3,'2016-11-25','20:00:00','Ligue 1 14 ème journée Bordeaux  vs Dijon',3,1 ),
  (4,'2016-11-25','20:00:00','Ligue 1 14 ème journée Caen  vs Guingamp ',0,1 ),
  (5,'2016-11-26','17:00:00','Ligue 1 14 ème journée Nice  vs Bastia',0,1 ),
  (6,'2016-11-26','15:00:00','Ligue 1 14 ème journée Anger  vs Saint-Etienne',19 ,1),
  (7,'2016-11-26','20:45:00','Ligue 1 14 ème journée OL  vs PSG',17,1 ),
  (8,'2016-11-29','19:00:00','Ligue 1 15 ème journée LOSC  vs Caen ',7,1 ),
  (9,'2016-11-29','19:00:00','Ligue 1 15 ème journée Lorient  vs Renne ',8,1 ),
  (10,'2016-11-29','21:00:00','Ligue 1 15 ème journée Dijon  vs Monaco ',0,1 ),
  (11,'2016-11-30','19:00:00','Ligue 1 15 ème journée Nante  vs OL ',9,1 ),
  (12,'2016-11-30','17:00:00','Ligue 1 15 ème journée Saint-Etienne  vs OM',0,1 ),
  (13,'2016-11-30','15:00:00','Ligue 1 15 ème journée PSG  vs Anger',17,1 ),
  (14,'2016-11-25','20:00:00','Pro A Chalons-Reims vs Le Mans',21,2),
  (15,'2017-11-18','20:00:00','Ligue 1 14 ème journée Lorient vs Monaco',NULL ,1),

  (16,'2017-12-02','20:45:00','Ligue 1 16 ème journée Caen vs Dijon',NULL,1),
  (17,'2017-12-03','17:00:00','Ligue 1 16 ème journée Montpellier vs PSG',NULL,1),
  (18,'2017-12-03','20:00:00','Ligue 1 16 ème journée Metz vs LYON',NULL,1),
  (19,'2017-12-04','15:00:00','Ligue 1 16 ème journée Rennes vs Saint-Etienne',NULL,1),
  (20,'2017-12-04','17:00:00','Ligue 1 16 ème journée OM vs Nancy',NULL,1),
  (21,'2017-12-04','20:45:00','Ligue 1 16 ème journée Nice vs Toulouse',NULL,1),
  (22,'2017-05-20','20:30:00','Gagnant du championnat de Ligue 1',NULL,1),
  (23,'2017-11-25','20:00:00','Pro A Chalons-Reims vs Le Mans',NULL,2)

    ;

INSERT INTO participe (idmatch,idteam,cote,score) VALUES
/*(1,'2016-11-25','20:45:00','Ligue 1 14 ème journée Toulouse  vs Renne ',18,1 ),*/
(1,20,3.20,0),
(1,18,2.35,1),
(1,0,3.0,NULL),
/*(2,'2016-11-26','17:00:00','Ligue 1 14 ème journée Monaco  vs OM ',12 ,1),*/
(2,0,3.75,NULL),
(2,12,1.62,4),
(2,10,5.25,0),
/*(3,'2016-11-25','20:00:00','Ligue 1 14 ème journée Bordeaux  vs Dijon',3,1 ),*/
(3,0,3.00,NULL),
(3,3,2.50,3),
(3,5,2.95,2),
/*(4,'2016-11-25','20:00:00','Ligue 1 14 ème journée Caen  vs Guingamp ',0,1 ),*/
(4,0,3.00,NULL),
(4,4,2.30,1),
(4,6,3.30,1),
/*(5,'2016-11-26','17:00:00','Ligue 1 14 ème journée Nice  vs Bastia',0,1 ),*/
(5,0,3.60,NULL ),
(5,16,1.65,1),
(5,2,5.25,1),
/*(6,'2016-11-26','15:00:00','Ligue 1 14 ème journée Anger  vs Saint-Etienne',19 ,1),*/
(6,0,2.90,NULL ),
(6,1,2.50,1),
(6,19,3.10,2),
/* (7,'2016-11-26','20:45:00','Ligue 1 14 ème journée OL  vs PSG',17,1 ),*/
(7,0,3.40,NULL ),
(7,9,3.75,1),
(7,17,1.95,2),
/*(8,'2016-11-29','19:00:00','Ligue 1 15 ème journée LOSC  vs Caen ',NULL,1 ),*/
(8,0,3.20,NULL ),
(8,7,1.75,4),
(8,4,5.25,2),
/* (9,'2016-11-29','19:00:00','Ligue 1 15 ème journée Lorient  vs Renne ',NULL,1 ),*/
(9,0,3.10,NULL ),
(9,8,2.65,2),
(9,18,2.70,1),
/*(10,'2016-11-29','21:00:00','Ligue 1 15 ème journée Dijon  vs Monaco ',NULL,1 ),*/
(10,0,3.30,NULL ),
(10,5,4.75,1),
(10,12,1.85,1),
/*(11,'2016-11-30','19:00:00','Ligue 1 15 ème journée Nante  vs OL ',NULL,1 ),*/
(11,0,3.10,NULL ),
(11,15,3.50,0),
(11,9,2.15,6),
/*(12,'2016-11-30','17:00:00','Ligue 1 15 ème journée Saint-Etienne  vs OM',NULL,1 ),*/
(12,0,3.0,NULL ),
(12,19,2.25,0),
(12,10,3.50,0),
/*(13,'2016-11-30','15:00:00','Ligue 1 15 ème journée PSG  vs Anger',NULL,1 ),*/
(13,0,6.50,NULL ),
(13,17,1.17,2),
(13,1,16.00,0),
/*(14,'2016-11-25','20:00:00','Pro A Chalons-Reims vs Le Mans',21,2),*/
(14,21,1.90,98),
(14,22,1.62,89),
/*(15,'2016-11-18','20:00:00','Ligue 1 13 ème journée Lorient vs Monaco',12,1)*/
(15,0,1.90,NULL ),
(15,12,5.0,3),
(15,8,2.5,0),
/*(16,'2016-12-02','20:45:00','Ligue 1 16 ème journée Caen vs Dijon',NULL,1)*/
(16,0,3.20,NULL ),
(16,4,2,NULL),
(16,5,3.70,NULL),
/* (17,'2016-12-03','17:00:00','Ligue 1 16 ème journée Montpellier vs PSG',NULL,1)*/
(17,0,2,NULL ),
(17,13,5.50,NULL),
(17,17,1.62,NULL),
/*(18,'2016-12-03','20:00:00','Ligue 1 16 ème journée Metz vs LYON',NULL,1)*/
(18,0,2,NULL ),
(18,11,5.0,NULL),
(18,9,1.70,NULL),
/*(19,'2016-12-04','15:00:00','Ligue 1 16 ème journée Rennes vs Saint-Etienne',NULL,1)*/
(19,0,2,NULL ),
(19,18,2.45,NULL),
(19,19,2.95,NULL),
/*(20,'2016-12-04','17:00:00','Ligue 1 16 ème journée OM vs Nancy',NULL,1)*/
(20,0,2,NULL ),
(20,10,1.75,NULL),
(20,14,4.75,NULL),
/*(21,'2016-12-04','20:45:00','Ligue 1 16 ème journée Nice vs Toulouse',NULL,1)*/
(21,0,3.10,NULL ),
(21,16,2.10,NULL),
(21,20,3.70,NULL),
/*(22,'2016-05-20','20:30:00','Gagnant du championnat de Ligue 1',NULL,1)*/
(22,1,751,NULL),
(22,2,751,NULL),
(22,3,71,NULL),
(22,4,501,NULL),
(22,5,1001,NULL),
(22,6,351,NULL),
(22,7,251,NULL),
(22,8,1001,NULL),
(22,9,24,NULL),
(22,10,121,NULL),
(22,11,1001,NULL),
(22,12,7.50,NULL),
(22,13,751,NULL),
(22,14,751,NULL),
(22,15,1001,NULL),
(22,16,9,NULL),
(22,17,1.18,NULL),
(22,18,201,NULL),
(22,19,121,NULL),
(22,20,201,NULL),

(23,21,1.90,NULL),
(23,22,1.90,NULL)

;

UPDATE participe
SET  score=NULL
WHERE idteam=0;

INSERT INTO parie (idParie, mise, dateParie, heureParie, idCompte, idmatch, idTeam) VALUES
  (1,10,curdate(),CURTIME(),1,16,4) ,
  (1,10,curdate(),CURTIME(),1,17,0),
  (1,10,curdate(),CURTIME(),1,18,0),
  (1,10,curdate(),CURTIME(),1,19,0),
  (1,10,curdate(),CURTIME(),1,20,0),
  (3,20,curdate(),CURTIME(),1,21,0) ,
  (2,20,curdate(),CURTIME(),1,22,0),
  (4,20,curdate(),CURTIME(),1,15,0),
  (6,20,curdate(),CURTIME(),2,15,0),
  (5,20,curdate(),CURTIME(),1,14,21)
  ;

