<?php

/**
 * Created by PhpStorm.
 * User: nneto
 * Date: 07/10/16
 * Time: 11:25
 */
class ModuleGenerique
{
    protected $controleur;

    /**
     * @return mixed
     */
    public function getControleur()
    {
        return $this->controleur;
    }


    public function vue_erreur($msg){
        $this->controleur->vue_erreur($msg);
    }
}