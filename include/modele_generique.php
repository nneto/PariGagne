 <?php

/**
 * Created by PhpStorm.
 * User: nneto
 * Date: 12/10/16
 * Time: 10:08
 */

class ModeleGenerique
{
    private static $dns="mysql:host=localhost;dbname=parisgagne";
   //"mysql:host=database-etudiants.iut.univ-paris8.fr;dbname=dutinfopw201651";
    private static $psw="";
                        //"rubajype";
    private static $usr="root";
          //"dutinfopw201651";
    protected  static  $connexion ;
    public static function  init(){
       self::$connexion=new PDO(self::$dns,self::$usr,self::$psw);
       self::$connexion->exec("SET NAMES 'UTF8'");
    }
    public function mdpCrypt($message,$login){
        $salt="jnrth4r8t4a464eraravsd";
        $salt2=substr($salt,0,strlen($salt)-strlen($login));
        $salt2.=$login;
        return  crypt($message,$salt2);
    }

    public function createToken($validiter ="300"){
        $token=$this->random(20);
        $r=self::$connexion->prepare("insert into token VALUES(?,now(),?)");
        $r->execute(array($token,$validiter));
        return $token;
    }

    public function getToken($token){
        $r=self::$connexion->prepare("select * from token where TIMESTAMPDIFF(SECOND,creation,now())<expiration  and token =?");
        $r->execute(array($token));
        return $r->fetch(PDO::FETCH_ASSOC);
    }

    public function effacerToken($token){
        $r=self::$connexion->prepare("delete from token  where token =?");
        $r->execute(array($token));
        $this->effacerTokenNonValide();
    }

    public function effacerTokenNonValide(){
        $r=self::$connexion->prepare("delete from token where TIMESTAMPDIFF(SECOND,creation,now())>expiration ");
        $r->execute();
    }
    public function random($car) {
        $string = "";
        $chaine = "abcdefghijklmnpqrstuvwxy";
        srand((double)microtime()*1000000);
        for($i=0; $i<$car; $i++) {
            $string .= $chaine[rand()%strlen($chaine)];
        }
        return $string;
    }
}