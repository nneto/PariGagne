<!DOCTYPE>
<html>
<head>
    <meta name="viewport" content="width=device-width"/>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> <?php echo $module->getControleur()->getVue()->getTitre();?></title>

    <script src="include/jquery-3.1.1.min.js"></script>

    <link href="bootstrap-3.3.7-dist/css/bootstrap-theme.min.css" rel="stylesheet">
    <link href="bootstrap-3.3.7-dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>

    <!-- Optional theme -->


    <?php
    $css=$module->getControleur()->getVue()->getCss();
    if(empty($css)){

    }else{


        foreach ($css as $cs){
            echo $cs.
                "\n";
        }
    }
    ?>
</head>
<?php
if(isset($_SESSION)&&isset($_SESSION['idcompte'])&&isset($_SESSION['login'])&&isset($_SESSION['solde']) && isset($_SESSION['admin'])) {
    if($_SESSION['admin']==1){
        include_once('navAdmin.php');
    }else{
        include_once('navCo.php');
    }

}else{
    include_once('navDeco.php');
}
;?>
<?php echo $module->getControleur()->getVue()->getContenu();?>
<!--footer start from here-->
<footer>
    <div class="container">
        <div class="row">
            <div class="footerleft ">
                <div class="logofooter col-md-2"><a href="accueil.html"><img src="source/logo.png" class="img-rounded" width="70" class="logo" > </a></div>
                <div class="col-md-4">
                    <p class="p">ParisGagne.com est un service en ligne de paris sportifs principalement centré sur le football, le basketball, le tennis. Vous pouvez dès à présent vous inscrire, si ce n'est pas déjà fait, et parier depuis chez vous.En ouvrant un compte ParisGagné, vous obtiendrez un bonus de bienvenue de 50 €. Ne perdez pas de temps et ajouter vos amis pour qu'il puisse voir tous vos paris.</p>
                </div>
                <div class="col-md-2 p">
                    <p><i class="fa fa-map-pin p"></i> IUT de montreuil 93000 Montreuil</p>
                    <p><i class="fa fa-phone p"></i> Phone : +337 78 69 43 96</p>
                    <p><i class="fa fa-envelope p"></i> E-mail:parisGagné@gmail.com</p>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 paddingtop-bottom">
                <h6 class="heading7 p">Liens utiles</h6>
                <ul class="footer-ul p">
                    <li><a href="contact.html"> Qui sommes nous ?</a></li>
                    <li><a href="#"> Nous contacter</a></li>
                    <li><a href="#"> Termes et conditions</a></li>
                    <li><a href="#"> Règles du pari sportif</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<div class="copyright">
    <div class="container">
        <div class="col-md-8">
            <p class="text-center">© 2016 - Copyright_All rights reserved_NETO-TOUATI-YELES</p>
        </div>
    </div>
</div>
</body>
</html>