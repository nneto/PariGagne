<?php  require_once('include/modele_generique.php');
                                            $m= new ModeleGenerique();?>

<article>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php"><img src="source/logo.png" class="img-rounded" width="50" class="logo" > </a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <li class="active"><a href="index.php?module=accueil">Accueil</a></li>
                <li><a href="index.php?module=matchs">Matchs</a></li>
                <li><a href="index.php?module=resultats">Résultats</a></li>
            </ul>
            <ul class=" row nav navbar-nav navbar-right">
                <li> <button onclick=" location.href = 'index.php?module=inscription'" type="button" id="myButton" data-loading-text="Loading..." class="btn btn-primary" autocomplete="off">
                        Inscription </button></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><b>Connexion</b> <span class="caret"></span></a>
                    <ul id="login-dp" class="dropdown-menu">
                        <li>
                            <div class="col-md-12">
                                <form class="form" role="form" method="post" action="index.php?module=connexion" accept-charset="UTF-8" id="login-nav">
                                    <div class="form-group">
                                        <input type="hidden" name="token" value='<?php
                                           echo $m->createToken();
                                        ?> '>
                                        <label class="sr-only" for="exampleInputEmail2">Pseudo</label>
                                        <input name="login" type="pseudo" class="form-control" id="exampleInputEmail2" placeholder="Pseudo" required>
                                    </div>
                                    <div class="form-group">
                                        <label class="sr-only" for="exampleInputPassword2">Mot de passe</label>
                                        <input name="mdp" type="password" class="form-control" id="exampleInputPassword2" placeholder="Mot de passe" required>
                                        <div class="help-block text-right"><a href="">Mot de passe oublié?</a></div>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-block">Connexion</button>
                                    </div>
                                </form>
                            </div>
        </div>
        </li>
        </ul>
    </div>
    </div>
</nav>