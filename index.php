<?php
/**
 * Created by PhpStorm.
 * User: max
 * Date: 31/10/2016
 * Time: 02:43
 */
require_once('include/vue_generique.php');
require_once('include/controleur_generique.php');
require_once('include/modele_generique.php');
require_once('include/module_generique.php');
include("include/modele_generique_excepetion.php");
include_once('module/mod_Refresh/mod_refresh.php');
session_start();
$modGen = new ModeleGenerique();
$modGen::init();
if(isset($_GET['module'])) {
    $nom_module = $_GET['module'];
}else{
    $nom_module = "accueil";
}

if(isset($_SESSION)&&isset($_SESSION['idcompte'])&&isset($_SESSION['login'])&&isset($_SESSION['solde']) && isset($_SESSION['admin'])) {
    if($_SESSION['admin']==0){
        $module=new ModRefresh();
        $module->refresh();
    }
}
switch ($nom_module) {
    case 'accueil':
        include_once('module/mod_accueil/mod_'.$nom_module.'.php');
        $nom_classe_module = "modaccueil";
        $module = new $nom_classe_module();
        $module->module_accueil();
        break;
    case 'inscription':
        include_once('module/mod_Inscription/mod_'.$nom_module.'.php');
        $nom_classe_module = "modinscription";
        $module = new $nom_classe_module();
        if(!(isset($_SESSION)&&isset($_SESSION['idcompte'])&&isset($_SESSION['login'])&&isset($_SESSION['solde']) && isset($_SESSION['admin']))){
            if (isset($_POST)&&isset($_POST['nom'])&& isset($_POST['prenom']) && isset($_POST['mdp']) && isset($_POST['mdp2']) && isset($_POST['pseudo']) && isset($_POST['date']) && isset($_POST['email'])&&isset($_POST['token'])){
                $module-> module_inscription($_POST);
            }else {
                $module->module_inscrire();
            }
        }else {
            $module->vue_erreur("non connecter");
        }
        break;
    case 'connexion':
        include_once ('module/mod_Connexion/mod_'.$nom_module.'.php');
        $nom_classe_module='modconnexion';
        $module=new $nom_classe_module();

        if(!(isset($_SESSION)&&isset($_SESSION['idcompte'])&&isset($_SESSION['login'])&&isset($_SESSION['solde'])&&isset($_SESSION['admin']))){
            if(isset($_POST['login'])&&isset($_POST['mdp'])&& isset($_POST['token'])){
                $module->module_connexion($_POST);
                header('Location:index.php?module=accueil');
            }
        }else{
            header('Location:index.php?module=accueil');
        }
        break;
    case 'deconnexion':
        session_destroy();
        header('Location:index.php?module=accueil');
        break;
    case 'matchs':
        include_once('module/mod_Matches/mod_matches.php');
        $nom_classe_module='modmatches';
        $module=new $nom_classe_module;
        if(isset($_GET['idmatche'])){
            $module->module_detail_matche($_GET['idmatche']);
        }else{
            if(isset($_POST['recherche'])){
                $module->module_matches_recherche($_POST['recherche']);
            }else{
                if(isset($_GET['idsport'])){
                    $module->module_matches_sport($_GET['idsport']);
                }else{
                    $module->module_matches();
                }

            }
        }
        break;
    case 'resultats':
        include_once('module/mod_resultats/mod_resultats.php');
        $nom_classe_module='modresultats';
        $module=new $nom_classe_module;
        if(isset($_GET['idMatch'])){
            $module->module_details_resultats($_GET['idMatch']);
        }else {
            if (isset($_POST['recherche'])) {
                $module->module_resultats_recherche($_POST['recherche']);
            } else {
                if(isset($_GET['idsport'])){
                    $module->module_matches_sport($_GET['idsport']);
                }else {
                    $module->module_resultats();
                }
            }
        }
        break;
    case 'parier':
        include_once('module/mod_Parier/mod_parier.php');
        $nom_classe_module='modparier';
        $module= new $nom_classe_module;
        if(isset($_SESSION)&&isset($_SESSION['idcompte'])&&isset($_SESSION['login'])&&isset($_SESSION['solde']) && isset($_SESSION['admin'])) {
            if($_SESSION['admin']==1){
                $module->vue_erreur("Un admin ne peut parier");
            }else {
                if (isset($_POST) && isset($_POST['equipe']) && isset($_POST['typeParie']) && isset($_POST['mise']) && $_POST['idMatch'] && $_POST['token']) {
                    if(isset($_POST['idParie'])){
                        $module->moduleParieEffectuer($_POST['equipe'], $_POST['typeParie'], $_POST['mise'], $_POST['idMatch'],$_POST['token'],$_POST['idParie'] );
                    }else{
                        $module->moduleParieEffectuer($_POST['equipe'], $_POST['typeParie'], $_POST['mise'], $_POST['idMatch'], $_POST['token']);
                    }
                } else {
                    $module->vue_erreur("Ereur 404 Page not found");
                }
            }
        }else{
            $module->vue_erreur("Ereur 404 Page not found");
        }
        break;
    case 'ajoutMatch':
        include_once('module/mod_Admin_AjoutMatches/mod_AjoutMatches.php');
        $nom_classe_module='ModAjoutAdmin';
        $module= new $nom_classe_module;
        if(isset($_SESSION)&&isset($_SESSION['idcompte'])&&isset($_SESSION['login'])&&isset($_SESSION['solde']) && isset($_SESSION['admin'])) {
            if($_SESSION['admin']==1){
                var_dump($_POST);
                if(isset($_POST['token'])&& isset($_POST['nbEquipe'])&&$_POST['datematch']&& isset($_POST['heurematch']) && isset($_POST['nommatch']) && isset($_POST['sport']) && isset($_POST['equipe1']) && isset($_POST['equipe2'])&&isset($_POST['cote2']  )&&isset($_POST['cote1'])){
                    $module->module_AjoutMatch($_POST);
                }else{
                    $module->module_AjoutAdmin();
                }
            }else{
                $module->vue_erreur("non admin");
            }
        }else{
            $module->vue_erreur("non connecter");
        }
        break;
    case 'profil':
        include_once('module/mod_profil/mod_profil.php');
        $nom_classe_module='ModProfil';
        $module= new $nom_classe_module;
        if(isset($_SESSION)&&isset($_SESSION['idcompte'])&&isset($_SESSION['login'])&&isset($_SESSION['solde']) && isset($_SESSION['admin'])) {
            if($_SESSION['admin']==0){
                $module->module_profil($_SESSION['idcompte']);
            }else{
                $module->vue_erreur("erreur admin ");
            }
        }else{
            $module->vue_erreur("non connecter");
        }
        break;
    case 'SuppMatche' :
        include_once('module/mod_Admin_SuppMatches/mod_SuppMatches.php');
        $nom_classe_module='ModSuppAdmin';
        $module= new $nom_classe_module;
        if(isset($_SESSION)&&isset($_SESSION['idcompte'])&&isset($_SESSION['login'])&&isset($_SESSION['solde']) && isset($_SESSION['admin'])) {
            if($_SESSION['admin']==1){
                if(isset($_GET['id'])){
                    $module->module_SuppMatches($_GET['id']);
                }else{
                    $module->vue_erreur("erreur id ");
                }
            }else{
                $module->vue_erreur("non admin");
            }
        }else{
            vue_erreur("non conecter");
        }
        break;
    case 'AjoutEquipe':
        include_once('module/mod_Admin_AjoutEquipe/mod_'.$nom_module.'.php');
        $nom_classe_module = "modajoutadmin";
        $module = new $nom_classe_module();
        if(isset($_SESSION)&&isset($_SESSION['idcompte'])&&isset($_SESSION['login'])&&isset($_SESSION['solde']) && isset($_SESSION['admin'])) {
            if($_SESSION['admin']==1){
                if(isset($_POST['nomteam'])){
                    $module->module_AjoutEquipe($_POST);
                }else{
                    $module->module_AjoutAdmin();
                }
            }else{
                $module->vue_erreur("non admin");
            }
        }else{
            vue_erreur("non connecter");
        }
        break;
    case 'ModifMatch':
        include_once('module/mod_Admin_ModifMatch/mod_'.$nom_module.'.php');
        $nom_classe_module = "ModModifAdmin";
        $module = new $nom_classe_module();
        if(isset($_SESSION)&&isset($_SESSION['idcompte'])&&isset($_SESSION['login'])&&isset($_SESSION['solde']) && isset($_SESSION['admin'])) {
            if($_SESSION['admin']==1) {
                if(isset($_GET['id'])){
                    $module->afficheForm($_GET['id']);
                }else{
                    if(isset($_POST['date'])&&isset($_POST['heure'])&&isset($_POST['nomMatch'])&&isset($_POST['gagnant'])&&isset($_POST['idMatch'])&&isset($_POST['token'])){
                        $module->modifMatch($_POST);
                    }else{
                        $module->vue_erreur("erreur");
                    }
                }
            }else{
                $module->vue_erreur("non admin");
            }
        }else{
            $module->vue_erreur("non connecter");
        }
        break;
    case 'parie':
        include_once('module/mod_Admin_Parie/mod_parie.php');
        $nom_classe_module='modparie';
        $module=new $nom_classe_module;
        if(isset($_SESSION)&&isset($_SESSION['idcompte'])&&isset($_SESSION['login'])&&isset($_SESSION['solde']) && isset($_SESSION['admin'])) {
            if($_SESSION['admin']==1) {
                $module->module_parie();
            }else{
                $module->vue_erreur("non admin");
            }
        }else{
            $module->vue_erreur("non connecter");
        }
        break;
    case 'compte':
        include_once('module/mod_Admin_Compte/mod_compte.php');
        $nom_classe_module='modcompte';
        $module=new $nom_classe_module;
        if(isset($_GET['idmatche'])){
            $module->module_detail_matche($_GET['idmatche']);
        }else{
            if(isset($_POST['recherche'])){
                $module->module_matches_recherche($_POST['recherche']);
            }else{
                $module->module_matches();
            }
        }
        break;
    case 'desinscrire':
        include_once('module/mod_profil/mod_profil.php');
        $nom_classe_module='ModProfil';
        $module= new $nom_classe_module;
        if(isset($_SESSION)&&isset($_SESSION['idcompte'])&&isset($_SESSION['login'])&&isset($_SESSION['solde']) && isset($_SESSION['admin'])) {
            if($_SESSION['admin']==0){
                if(isset($_POST['token'])){
                    $module->module_desinscrire($_SESSION['idcompte'],$_POST['token']);
                }else{

                    $module->vue_erreur("erreur");
                }
            }else{
                $module->vue_erreur("erreur admin ");
            }
        }else{
            $module->vue_erreur("non connecter");
        }
        break;
    case 'modifMdp':
        include_once('module/mod_modifProfil/mod_modifProfil.php');
        $nom_classe_module='ModModifProfil';
        $module= new $nom_classe_module;
        if(isset($_SESSION)&&isset($_SESSION['idcompte'])&&isset($_SESSION['login'])&&isset($_SESSION['solde']) && isset($_SESSION['admin'])) {
            if ($_SESSION['admin'] == 0) {
                if(isset($_POST['token'])&&isset($_POST['oldpsw'])&&isset($_POST['newpsw'])&&isset($_POST['confirmpsw'])){
                    $module->moduleChangeMdp($_SESSION['idcompte'],$_POST);
                }else{
                    $module->moduleFormMdp();
                }
            }else{
                $module->vue_erreur("erreur admin ");
            }
        }else{
            $module->vue_erreur("erreur non connecter ");
        }
        break;
    case 'modifPseudo':
        include_once('module/mod_modifProfil/mod_modifProfil.php');
        $nom_classe_module='ModModifProfil';
        $module= new $nom_classe_module;
        if(isset($_SESSION)&&isset($_SESSION['idcompte'])&&isset($_SESSION['login'])&&isset($_SESSION['solde']) && isset($_SESSION['admin'])) {
            if ($_SESSION['admin'] == 0) {
                if(isset($_POST['token'])&&isset($_POST['pseudo'])){
                    $module->moduleChangePseudo($_SESSION['idcompte'],$_POST);
                }else{
                    $module->moduleFormPseudo();
                }
            }else{
                $module->vue_erreur("erreur admin ");
            }
        }else{
            $module->vue_erreur("erreur non connecter");
        }
        break;
    case 'modifEmail':
        include_once('module/mod_modifProfil/mod_modifProfil.php');
        $nom_classe_module='ModModifProfil';
        $module= new $nom_classe_module;
        if(isset($_SESSION)&&isset($_SESSION['idcompte'])&&isset($_SESSION['login'])&&isset($_SESSION['solde']) && isset($_SESSION['admin'])) {
            if ($_SESSION['admin'] == 0) {

                if(isset($_POST['token'])&&isset($_POST['email'])){
                    $module->moduleChangeEmail($_SESSION['idcompte'],$_POST);
                }else{
                    $module->moduleFormEmail();
                }
            }else{
                $module->vue_erreur("erreur admin ");
            }
        }else{
            $module->vue_erreur("erreur admin ");
        }
        break;
    case 'AjoutSport':
        include_once('module/mod_Admin_AjoutSport/mod_'.$nom_module.'.php');
        $nom_classe_module = "modajoutadmin";
        $module = new $nom_classe_module();
        if(isset($_SESSION)&&isset($_SESSION['idcompte'])&&isset($_SESSION['login'])&&isset($_SESSION['solde']) && isset($_SESSION['admin'])) {
            if($_SESSION['admin']==1){
                if(isset($_POST['nomteam'])){
                    var_dump("test");
                    $module->module_AjoutEquipe($_POST);
                }else{
                    $module->module_AjoutAdmin();
                }
            }else{
                $module->vue_erreur("non admin");
            }
        }else{
            vue_erreur("non connecter");
        }
        break;
    default:
        header('Location:index.php?module=accueil');
        break;
}

require_once('include/template.php');
