<?php

/**
 * Created by PhpStorm.
 * User: nneto
 * Date: 18/11/16
 * Time: 11:23
 */
include_once('modele_compte.php');
include_once('vue_compte.php');
class ControleurCompte extends ControleurGenerique
{   /**
 * ControleurMatches constructor.
 */
    public function __construct()
    {
        $this->modele = new ModelCompte();
        $this->vue = new VueCompte();
    }
    public function listeMatches(){
        $matches=$this->modele->getMatches();
        if(empty($matches)){
            $this->vue->vue_erreur("aucun compte a afficher");
        }else{
            $this->vue->afficheMatches($matches);
        }
    }



}
