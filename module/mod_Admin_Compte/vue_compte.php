<?php

/**
 * Created by PhpStorm.
 * User: nneto
 * Date: 18/11/16
 * Time: 11:23
 */
class VueCompte extends VueGenerique
{
    public function afficheMatches($matches){

        $cpt=0;
        $this->titre="compte";
        $this->Css=array("<link href=\"module/mod_Matches/matchs.css\" rel=\"stylesheet\" type=\"text/css\">");
        $this->contenu.="	<article>
			<div class=\"row\" id=\"main\">
				<h2  id=\"titre\"> Liste des comptes </h2>
				<hr/>
					<div class=\"col-md-2\" >
						<div class=\"list-group\">
							
						";

        $this->contenu.= "</div>
					</div>
					<div   class=\"col-md-6\">";
        foreach ($matches as $compte){
            $pseudo= htmlspecialchars($compte['pseudo']);
            $email= htmlspecialchars($compte['email']);
            $solde= htmlspecialchars($compte['solde']);
            $this->contenu.= " 
						<div class=\"panel panel-default\">
						  	<div class=\"panel-heading\">
						    		<h3 style='text-align: center;font-weight: bold'class=\"panel-title\">
						    		$pseudo
						    		</h3>	
						    </div>";
                        $this->contenu .= "<div class=\"panel-body list-group - item - text\">
                       
                        ";
                        if($_SESSION['admin']==1){
                            $this->contenu .= "email : $email<br>";
                            $this->contenu .= "solde : $solde";
                        }
                        $this->contenu.="</div></div>";
            }
        $this->contenu.= "	<div class=\"row\" class=\"aside\">
                            </div>
                </article>
                    <script src='module\mod_Matches\parier.js' type='text/javascript'></script>
                    <script src='module\mod_Matches\bootbox.min.js' type='text/javascript'></script>
                    
                     ";
        }
}
