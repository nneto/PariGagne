<?php

/**
 * Created by PhpStorm.
 * User: NICO
 * Date: 15/01/2017
 * Time: 23:14
 */
include_once ('vue_ModifMatch.php');
include_once ('modele_ModifMatch.php');
class ControleurAdminModifMatch extends ControleurGenerique
{

    /**
     * ControleurAdminModifMatch constructor.
     */
    public function __construct()
    {
        $this->vue=new VueModifAdmin();
        $this->modele= new ModelModifAdmin();
    }

    public function afficheForm($id){
        $token=$this->modele->createToken();
        $detailsMatch=$this->modele->getDetailsMatchs($id);
        $participant=$this->modele->getParticipantMatch($id);
        if($detailsMatch==null || $participant==null){
            $this->vue->vue_erreur("404 Page NOT FOUND");
        }else{
            $this->vue->modifMatch($detailsMatch[0],$participant,$token);
        }
    }

    public function modifMatch($tab){
        $r=$this->modele->getToken($tab['token']);
        if(empty($r)){
            $this->vue->vue_erreur("formulaire expirer");
        }else{
            $this->modele->effacerToken($tab['token']);
            ($tab['gagnant']);
            if($tab['gagnant']=='NULL'){
                $this->modele->updateMatch($tab);
                header('Location:index.php?module=matchs');
            }else{
                $this->modele->updateMatch2($tab);
                for ($i=0;$i<$tab['nbParticipant'];$i++){
                    $this->modele->ajouterScore($tab["id$i"],$tab["score$i"],$tab['idMatch']);
                }
                $this->modele->gagner($tab['gagnant'],$tab['idMatch']);
                header('Location:index.php?module=matchs');
            }
        }
    }
}