<?php

/**
 * Created by PhpStorm.
 * User: NICO
 * Date: 15/01/2017
 * Time: 23:17
 */
class ModelModifAdmin extends ModeleGenerique
{

    public function getDetailsMatchs($idMatche){
        $requeteDetailMatch="select * from matchs where idmatch=?";
        $result=self::$connexion->prepare($requeteDetailMatch);
        $result->execute(array($idMatche));
        return $result->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getParticipantMatch($idMatch){
        $requeteparticipant="select * from participe natural join equipe where idMatch=? order by idTeam desc";
        $result=self::$connexion->prepare($requeteparticipant);
        $result->execute(array($idMatch));
        return $result->fetchAll(PDO::FETCH_ASSOC);
    }

    public function updateMatch($tab){

        $requete="update  matchs set nomMatch=?,dateMatch=?,heureMatch=?,gagnant=NULL where idMatch=?";
        $result=self::$connexion->prepare($requete);
        $date = new DateTime($tab['date']);
        $d=  $date->format('Y-m-d');
        $result->execute(array($tab['nomMatch'],$d,$tab['heure'],$tab['idMatch']));
    }
    public function updateMatch2($tab){

        $requete="update  matchs set nomMatch=?,dateMatch=?,heureMatch=?,gagnant=? where idMatch=?";
        $result=self::$connexion->prepare($requete);
        $date = new DateTime($tab['date']);
        $d=  $date->format('Y-m-d');
        $result->execute(array($tab['nomMatch'],$d,$tab['heure'],$tab['gagnant'],$tab['idMatch']));
    }

    public function ajouterScore($equipe,$score,$idmatch){
        $requete="update  participe set score=? where idMatch=? and idTeam=?";
        $result=self::$connexion->prepare($requete);
        $result->execute(array($score,$idmatch,$equipe));
    }
    public function gagner($gagner,$idMatch){
        $this->setGagnant($gagner,$idMatch);
        $paris=$this->getParieGagnant($idMatch);
        foreach($paris as $pari ){
            if($this->isGagnant($pari['idParie'])){
                $this->setSolde($pari['idParie']);
            }
        }
    }

    private function isGagnant($idParie){
        $requete="select  * from parie where idParie=?";
        $result=self::$connexion->prepare($requete);
        $result->execute(array($idParie));
        $paris=$result->FetchAll(PDO::FETCH_ASSOC);
        foreach ($paris as $pari){
            if($pari['gagne']==0){
                return false;
            }
        }
        return true;
    }
    private function setSolde($idParie){
        $miseTotal=0;
        $coteTotal=1;
        $compte="";
        $requete="select  * from parie natural join participe where idParie=?";
        $result=self::$connexion->prepare($requete);
        $result->execute(array($idParie));
        $paris=$result->FetchAll(PDO::FETCH_ASSOC);
        foreach ($paris as $pari){
            $miseTotal+=$pari['mise'];
            $coteTotal*=$pari['cote'];
            $compte=$pari['idCompte'];
        }
        $gaintTotal=$miseTotal*$coteTotal;
        $requete="update compte set solde=solde+? where idCompte=?";
        $result=self::$connexion->prepare($requete);
        $result->execute(array($gaintTotal,$compte));
    }
    private function setGagnant($gagnant,$idMatch){
        $requete=" UPDATE parie set gagne=true where idTeam=? and gagne is null  and idmatch=?";
        $result=self::$connexion->prepare($requete);
        $result->execute(array($gagnant,$idMatch));
        $requete="UPDATE parie set gagne=false where idTeam<>? and gagne is null and idmatch=?";
        $result=self::$connexion->prepare($requete);
        $result->execute(array($gagnant,$idMatch));
    }

    private function getParieGagnant($idMatch){

        $requete="select * from parie where idMatch=? and gagne=true ORDER by idParie";
        $result=self::$connexion->prepare($requete);
        $result->execute(array($idMatch));
        $t=$result->FetchAll(PDO::FETCH_ASSOC);
        return $t;
    }
}