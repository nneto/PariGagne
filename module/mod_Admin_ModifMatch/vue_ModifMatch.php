<?php

/**
 * Created by PhpStorm.
 * User: NICO
 * Date: 15/01/2017
 * Time: 23:29
 */
class VueModifAdmin extends VueGenerique
{
    public function modifMatch($detail,$particpants,$token){
       $n=sizeof($particpants);
        $cpt=0;
        $this->titre="modif";
        $dateMatch=htmlspecialchars($detail['dateMatch']);
        $heureMatch=htmlspecialchars($detail['heureMatch']);
        $nomMatch=htmlspecialchars($detail['nomMatch']);
        $this->contenu="
        <form action= 'index.php?module=ModifMatch' method='post' style=\"width:80%;height:20%;font-weight: bold;font-size:80%\">
            <input type='hidden' name='token' value='$token'>
            <input type='hidden' name='idMatch' value='$_GET[id]'>
            <input type='hidden' name='nbParticipant' value='$n'>n
            <label>date du match  </label> :<input id='date' style=\"border-radius: 2px;border: solid 1px #006dcc\" value='$dateMatch' type=\"text\" name='date'required>
                <script id=\"script\"  >
					$(function() {
						$( \"#date\" ).datepicker({
							showMonthAfterYear: false,
							inline : true,
							changeMonth:true,
							changeYear:true,
						    minDate:0});
					});
						</script>
            <label>heure du match </label> :<input id='time' style=\"border-radius: 2px;border: solid 1px #006dcc\"  value='$heureMatch' type=\"text\" name='heure' required>
                    <script id=\"script\">
					$(function() {
						$('#time').timepicker({
                        'timeFormat': 'H:i:s',
                        step:15
						});
					});
					</script >
            <label>nom du match  </label> :<input style=\"border-radius: 2px;border: solid 1px #006dcc\" value='$nomMatch' type=\"text\" name='nomMatch' required>
            
            <label> veuillez choisir un gagnant : </label><br/>
            ";
        foreach ($particpants as $particpant){
            $nomTeam=htmlspecialchars($particpant['nomTeam']);
            $this->contenu.="  <input type='hidden' value='$particpant[idteam]' name='id$cpt'>
          <input class='equipe'  name='gagnant' type=\"radio\" value=\"$particpant[idteam]\" ><label>$nomTeam</label> 
                  <input type='number' min='0'  name='score$cpt'>
            ";
            $cpt++;
        }
        $this->contenu.="
    <input  name='gagnant' type=\"radio\" class='test' id='nope' value=\"NULL\" checked ><label>Pas encore de gagnant</label>
           <input style=\"background-color: #006dcc;border: solid 2px #006dcc;border-radius:2px;color:white\"type=\"submit\" id=\"\" name=\"\" value=\"Modifier\"required><br/>        
        </form>
            
        ";
        $this->Css=array(
            "<link rel='stylesheet' href='//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css'>",
            "<script src='https://code.jquery.com/jquery-1.12.4.js'></script>",
            "<link href='module/mod_Inscription/css/datepicker.css' rel='stylesheet' type='text/css'/>",
            "<script src='https://code.jquery.com/ui/1.12.1/jquery-ui.js'></script>",
            "<script src='include/timepicker/jquery.timepicker.min.js'></script>",
            "<script src='include/timepicker/GruntFile.js'></script>",
            "<link rel='stylesheet' href='include/timepicker/jquery.timepicker.css'>",
            "<script src='module/mod_Admin_ModifMatch/modifMatch.js'></script>"
        )
        ;
    }
}