<?php

/**
 * Created by PhpStorm.
 * User: NICO
 * Date: 15/01/2017
 * Time: 23:22
 */
include_once ('controleur_ModifMatch.php');
class ModModifAdmin extends ModuleGenerique
{


    /**
     * ModModifAdmin constructor.
     */
    public function __construct()
    {
        $this->controleur=new ControleurAdminModifMatch();
    }

    public function afficheForm($id){

        $this->controleur->afficheForm($id);
    }

    public function modifMatch($tab){
        $this->controleur->modifMatch($tab);
    }

}