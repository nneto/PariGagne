<?php
/**
 * Created by PhpStorm.
 * User: nacer
 * Date: 22/01/2017
 * Time: 04:18
 */
class VueParie extends VueGenerique
{
    public function afficheParie($matches){
        $cpt=0;
        $this->titre="parie";
        $this->Css=array("<link href=\"module/mod_Matches/matchs.css\" rel=\"stylesheet\" type=\"text/css\">");
        $this->contenu.="	<article>
			<div class=\"row\" id=\"main\">
				<h2  id=\"titre\"> Liste des paris </h2>
				<hr/>
					<div class=\"col-md-2\" >
						<div class=\"list-group\">";
        $this->contenu.= "</div>
					</div>
					<div   class=\"col-md-6\">";
        foreach ($matches as $compte){
            $nomMatch=htmlspecialchars($compte['nomMatch']);
            $pseudo=htmlspecialchars($compte['pseudo']);
            $equipe=htmlspecialchars($compte['nomTeam']);
            $cote=htmlspecialchars($compte['cote']);
            $mise=htmlspecialchars($compte['mise']);
            $date=htmlspecialchars($compte['dateParie']);
            $heure=htmlspecialchars($compte['heureParie']);
            $email=$compte['email'];
            $this->contenu.= " 
						<div class=\"panel panel-default\">
						  	<div class=\"panel-heading\">
						    		<h3 style='text-align: center;font-weight: bold' class=\"panel-title\">
						    		 $nomMatch
						    		</h3>	
						    </div>";
            $this->contenu .= "<div class=\"panel-body list-group - item - text\">  ";
            $this->contenu .= " pseudo : $pseudo <br>";
            $this->contenu .= " equipe : $equipe <br>";
            $this->contenu .= " cote : $cote <br>";
            $this->contenu .= "mise : $mise <br>";
            $this->contenu .= "date du parie : $date<br>";
            $this->contenu .= "heure du parie : $heure <br>";
            $this->contenu .= "email : $email";
            $this->contenu.="</div></div>";
        }
        $this->contenu.= "	<div class=\"row\" class=\"aside\">
                            </div>
                </article>
                     ";
    }
}
