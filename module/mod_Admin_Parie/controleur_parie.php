<?php
/**
 * Created by PhpStorm.
 * User: nacer
 * Date: 22/01/2017
 * Time: 04:17
 */
include_once('modele_parie.php');
include_once('vue_parie.php');
class ControleurParie extends ControleurGenerique
{   /**
 * ControleurMatches constructor.
 */
    public function __construct()
    {
        $this->modele = new ModelParie();
        $this->vue = new VueParie();
    }
    public function listeParie(){
        $matches=$this->modele->getParie();
        if(empty($matches)){
            $this->vue->vue_erreur("aucun compte a afficher");
        }else{
            if($_SESSION['admin']==1) {
                $this->vue->afficheParie($matches);
            }
        }
    }



}