<?php
/**
 * Created by PhpStorm.
 * User: nacer
 * Date: 22/12/2016
 * Time: 17:34
 */
class VueAjoutAdmin extends VueGenerique{

    public function vue_afficheForm($token,$equipes,$sports){
        $this->titre="AjouterMatches";
        $this->contenu="<form method=\"post\" action=\"index.php?module=ajoutMatch\" style='font-weight:bold;font-size:90%;width:80%;margin-left:10%'>
               <input type='hidden' name='token'  value='$token' >
                <input type='hidden' name='nbEquipe'id='nbEquipe' value='2' >
                      <label>date du match : </label> 
                     
                         <script id=\"script\"  >
					$(function() {
						$( \"#date\" ).datepicker({
							showMonthAfterYear: false,
							inline : true,
							changeMonth:true,
							changeYear:true,
						    minDate:0
							
						});
					});
						</script>
			<input  style='border-radius:5px;border: 2px solid black'type=\"text\" name='datematch' id='date' min=\"2017-01-04\" max=\"2017-12-25\"required>
        <label>heure du match : </label>
         <input style='border-radius:5px;border: 2px solid black' type=\"text\" id='time' name='heurematch'required>
                         <script id=\"script\">
					$(function() {
						$('#time').timepicker({
                        'timeFormat': 'H:i:s',
                        step:15      
						});
					});
						</script>
         
         <label>nom du match : </label> :<input style='border-radius:5px;border: 2px solid black' type=\"text\" name='nommatch'required>
     
        Sport du match :
        <select style='border-radius:5px;border: 2px solid black' name='sport' size='1'>
        ";

        foreach ($sports as $sport){

            $this->contenu.="<option>$sport[nomSport]";
        }
        $this->contenu.="
            </select>
            
       
        <div id='equipes'>
        <p id='equipe1'>
        
        <div style='font-weight: bold;font-size:90%;margin-left:35%'>
        Participant du matche :
        <select style='margin-bottom:1%;margin-left:2.2%;width:25%;height:3%;border-radius:5px;border: 2px solid black'name='equipe1' size='1'>
        
         ";
        foreach ($equipes as $equipe){
            $this->contenu.="<option>$equipe[nomTeam]";
        }

        $this->contenu.="
         </select>
    ><br>
    cote de l'equipe 1:
    <input name='cote1' style='margin-top:2%;width:25%;height:3%;border-radius:5px;border: 2px solid black' type='number' step=\"0.1\" value=\"1\" min=\"1\">
    </p>
            <p id='equipe2'>
     
        Participant du matche :
        <select style='margin-top:2%;margin-left:2.2%;width:25%;height:3%;border-radius:5px;border: 2px solid black'name='equipe2' size='1'>
         ";
        foreach ($equipes as $equipe){
            $this->contenu.="<option>$equipe[nomTeam]";
        }
        $this->contenu.="
           </select>
  
     <br>cote de l'equipe 2 : <input name='cote2' style='margin-top:2%;width:25%;height:3%;border-radius:5px;border: 2px solid black' type='number' step=\"0.1\" value=\"1\" min=\"1\">
   
    </div>
    </div>
    	<p ><type type='button' style='margin-top:4%;margin-left: 42%'  class='btn btn-primary btn-sm ' onclick='ajouterEquipes()' >ajouter un participant</type></p>
          <script src='module/mod_Admin_AjoutMatches/ajoutMatch.js'></script>
    <input style='margin-left:42%' class=\"btn btn-primary btn-sm \" type=\"submit\" id=\"submit\" name=\"ajouter\" value=\"ajouter match\"required>
  </form>
    ";
        $this->Css=array(
            "<link rel='stylesheet' href='//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css'>",
            "<script src='https://code.jquery.com/jquery-1.12.4.js'></script>",
            "<link href='module/mod_Inscription/css/datepicker.css' rel='stylesheet' type='text/css'/>",
            "<script src='https://code.jquery.com/ui/1.12.1/jquery-ui.js'></script>",
            "<script src='include/timepicker/jquery.timepicker.min.js'></script>",
            "<script src='include/timepicker/GruntFile.js'></script>",
            "<link rel='stylesheet' href='include/timepicker/jquery.timepicker.css'>"
        )
        ;


    }
}