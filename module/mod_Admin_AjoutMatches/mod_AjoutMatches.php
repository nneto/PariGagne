<?php
/**
 * Created by PhpStorm.
 * User: nacer
 * Date: 22/12/2016
 * Time: 17:33
 */
include_once('controler_AjoutMatches.php');
class ModAjoutAdmin extends ModuleGenerique
{
    public function module_AjoutAdmin(){

        $this->controleur=new ControleurAdminAjoutMatches();
        $this->controleur->controleur_initForm();

    }

    public function module_AjoutMatch($tab){
        $this->controleur= new ControleurAdminAjoutMatches();
        $this->controleur-> controler_adminMatch($tab);
    }

}