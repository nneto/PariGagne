<?php
/**
 * Created by PhpStorm.
 * User: nacer
 * Date: 22/12/2016
 * Time: 17:33
 */
class ModelAjoutAdmin extends ModeleGenerique
{

    public function getSports(){
        $requeteSports="select * from sport  ";
        $result=self::$connexion->query($requeteSports);
        return $result->fetchall(PDO::FETCH_ASSOC);
    }

    public function getEquipes(){
        $requeteSports="select * from equipe order by idteam";
        $result=self::$connexion->query($requeteSports);
        return $result->fetchall(PDO::FETCH_ASSOC);
    }

    public function getSport($sport){
        $requet="select * from sport where nomSport=? limit 1";
        $requete=self::$connexion->prepare($requet);
        $requete->execute(array($sport));
        $t=$requete->fetchall(PDO::FETCH_ASSOC);
        return $t;
    }
    public function addMatches($datematch,$heurematch,$nommatch,$sport){
        $idSport=$this->getSport($sport);
        $requeteAjout="INSERT INTO matchs(dateMatch,heureMatch,nomMatch,gagnant,idSport) VALUES (?,?,?,NULL ,?)";
        $requete=self::$connexion->prepare($requeteAjout);
        $requete->execute(array($datematch,$heurematch,$nommatch,$idSport[0]['idSport']));
    }

    public function getEquipe($equipe){
        $requet="select * from equipe where nomTeam=? limit 1";
        $requete=self::$connexion->prepare($requet);
        $requete->execute(array($equipe));
        $t=$requete->fetchall(PDO::FETCH_ASSOC);
        return $t;
    }
    public function getLastMatch(){
        $requeteSports="select max(idmatch) from matchs ";
        $result=self::$connexion->query($requeteSports);
        return $result->fetchall(PDO::FETCH_ASSOC);
    }
    public function addParticipant($equipe,$cote){
        $idMatch=$this->getLastMatch()[0]['max(idmatch)'];

        $idEquipe=$this->getEquipe($equipe)[0]['idteam'];
        $requeteAjout="INSERT INTO participe VALUES (?,?,?,NULL)";
        $requete=self::$connexion->prepare($requeteAjout);
        $requete->execute(array($cote,$idEquipe,$idMatch));
    }
}