<?php
/**
 * Created by PhpStorm.
 * User: nacer
 * Date: 22/12/2016
 * Time: 17:34
 */
require_once('modele_AjoutMatches.php');
require_once('vue_AjoutMatches.php');

class ControleurAdminAjoutMatches extends ControleurGenerique
{
    /**
     * ControleurInscription constructor.
     */
    public function __construct()
    {
        $this->modele=new ModelAjoutAdmin();
        $this->vue=new VueAjoutAdmin();
    }

    public function controleur_initForm(){
        $sports=$this->modele->getSports();
        $equipes=$this->modele->getEquipes();
        $token=$this->modele->createToken();
        $this->vue->vue_afficheForm($token,$equipes,$sports);
    }

    public function controler_adminMatch($tab)
    {
        var_dump("test");
        $equipes=array();
        $cotes=array();
        $r=$this->modele->getToken($tab['token']);
        try{
            if(empty($r)){
                $this->vue->vue_erreur("formulaire expirer");
            }else {
                $this->modele->effacerToken($tab['token']);
                for ($i = 1; $i <= $tab['nbEquipe']; $i++) {
                    if (in_array($tab['equipe' . $i], $equipes)) {

                        throw new ModeleGeneriqueException();
                    } else {
                        array_push($equipes, $tab['equipe' . $i]);
                        array_push($cotes , $tab['cote' . $i]);
                    }
                }
            }
            $date = new DateTime($tab['datematch']);
            $date=  $date->format('Y-m-d');
            $this->modele->addMatches($date,$tab['heurematch'],$tab['nommatch'],$tab['sport']);
            for($i = 1; $i <= $tab['nbEquipe']; $i++) {
                $this->modele->addParticipant($tab['equipe' . $i],$tab['cote'. $i]);
            }
            header('Location:index.php?module=matchs');
        }catch (ModeleGeneriqueException $e){
            $this->vue->vue_erreur("Erreur ajout match");
        }
    }




}