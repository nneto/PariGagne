<?php

/**
 * Created by PhpStorm.
 * User: nneto
 * Date: 15/11/16
 * Time: 16:22
 */
class VueAccueil extends VueGenerique
{
    public function vue_Accueil(){
        $this->titre="ParisGagné";
        $this->Css=array("<link href=\"module/mod_accueil/accueil.css\" rel=\"stylesheet\" type=\"text/css\">");
        $this->contenu=" 	
 			 <style>
		  .carousel-inner > .item > img,
		  .carousel-inner > .item > a > img {
            width: 70%;
            margin: auto;
        }
		  <style>
			    /* Remove the navbar's default margin-bottom and rounded borders */
			    .navbar {
            margin-bottom: 0;
			      border-radius: 0;
			    }

			    /* Add a gray background color and some padding to the footer */
			    footer {
            background-color: #f2f2f2;
			      padding: 25px;
			    }

			  .carousel-inner img {
            width: 100%; /* Set width to 100% */
            margin: auto;
            min-height:200px;
			  }

			  /* Hide the carousel text when the screen is less than 600 pixels wide */
			  @media (max-width: 600px) {
            .carousel-caption {
                display: none;
            }
			  }
		  </style>
  	</style>
 			<article>
 					<!-- /.container -->
		<div class=\"row\" id=\"main\" >
			<div class=\"col-md-7\" >
				<div id=\"my_carousel\" class=\"carousel slide\" data-ride=\"carousel\"  >
			<!-- Bulles -->
					<ol class=\"carousel-indicators\">
						<li data-target=\"#my_carousel\" data-slide-to=\"0\" class=\"active\"></li>
						<li data-target=\"#my_carousel\" data-slide-to=\"1\"></li>
						<li data-target=\"#my_carousel\" data-slide-to=\"2\"></li>
					</ol>
					<!-- Slides -->
					<div class=\"carousel-inner\">
						<!-- Page 1 -->
						<div class=\"item active\">  
							<div class=\"carousel-page\">		
								<img src=\"source/Rudby.jpg\" class=\"img-responsive\" style=\"margin:0px auto;\" /> 
							</div> 
							<div class=\"carousel-caption\"></div>
						</div>   
						<!-- Page 2 -->
						<div class=\"item\"> 
							<div class=\"carousel-page\"><img src=\"source/tennis.jpg\" class=\"img-responsive img-rounded\" style=\"margin:0px auto;\"/></div> 
							<div class=\"carousel-caption\"></div>
						</div>  
						<!-- Page 3 -->
						<div class=\"item\">  
							<div class=\"carousel-page\">
								<img src=\"source/parierFoot.jpg\" class=\"img-responsive img-rounded\" style=\"margin:0px auto;max-height:100%;\"/>
							</div>  
							<div class=\"carousel-caption\"></div>
						</div>     
					</div>
					<!-- Contrôles -->
					<a class=\"left carousel-control\" href=\"#my_carousel\" data-slide=\"prev\">
						<span class=\"glyphicon glyphicon-chevron-left\"></span>
					</a>
					<a class=\"right carousel-control\" href=\"#my_carousel\" data-slide=\"next\">
						<span class=\"glyphicon glyphicon-chevron-right\"></span>
					</a>
				</div>
						
			</div>
			<!-- JUMBOTRON -->
			<div class=\"col-md-5\" class=\"jumbotron\" id=\"jumbotron\">
				<div class=\"jumbotron\"  class=\"img-responsive img-rounded\" style=\"margin:0px auto;\">
  					<h2>Venez Parier !</h2>
  					<p > <small>Démarrer avec 50 Euros offert, et parier sur les derniers matchs, avec un max de chance de gagner. </small></p>
 				 	<p><a class=\"btn btn-primary\" href=\"#\" role=\"button\">Inscrivez-vous ici</a></p>
				</div>

			</div> 
		</div>
			<script>
				$(document).ready(function(){
				    // Activate Carousel
				    $(\"#myCarousel\").carousel();
				    
				    // Enable Carousel Indicators
				    $(\".item1\").click(function(){
					$(\"#myCarousel\").carousel(0);
				    });
				    $(\".item2\").click(function(){
					$(\"#myCarousel\").carousel(1);
				    });
				    $(\".item3\").click(function(){
					$(\"#myCarousel\").carousel(2);
				    });
				    $(\".item4\").click(function(){
					$(\"#myCarousel\").carousel(3);
				    });
				    
				    // Enable Carousel Controls
				    $(\".left\").click(function(){
					$(\"#myCarousel\").carousel(\"prev\");
				    });
				    $(\".right\").click(function(){
					$(\"#myCarousel\").carousel(\"next\");
				    });
				});
				</script>
		</article>
				
				<hr/>
				<section>
					<div class=\"container text-center\">
  						<h3 class=\"col-md-10\">Actualités</h3><br>
					</div>
					<div class=\"row\">
						  <div class=\"col-sm-6 col-md-4\">
						    <div class=\"thumbnail\">
						      <img src=\"source/russia.jpg\" alt=\"russia\" class=\"img-circle\">
						      <div class=\"caption\">
							<h3>France-Suède: Tous savoir pour bien parier !</h3>
							<p>Vendredi, les Bleus reçoivent la Suède au Stade de France pour le compte des qualifications au mondial 2018. Qu’attendre de la rencontre ? Quels paris tenter ? Sportytrader a fait le tour de la question.</p>
							<p><a href=\"http://www.sportytrader.com/meilleurs-paris-france_suede-1975.htm\" class=\"btn btn-primary\"\" class=\"btn btn-default\" role=\"button\">En savoir plus</a> 
						      </div>
						    </div>
						  </div>

						<div class=\"col-sm-6 col-md-4 \">
						    <div class=\"thumbnail\">
						      <img src=\"source/champion.png\" alt=\"champion\" class=\"img-circle\">
						      <div class=\"caption\">
							<h3>LDC : Quelle semaine pour les clubs français ?</h3>
							<p>Un déplacement à Bâle pour Paris, une réception du CSKA Moscou pour Monaco et un voyage compliqué pour l’OL au Juventus Stadium, tel est le programme des clubs tricolores en Ligue des Champions cette semaine. Quelles rencontres attendre ? Sur quoi miser ? Tour d’horizon des meilleurs paris à tenter.</p>
							<p></a> <a href=\"http://www.sportytrader.com/clubs_francais-ligue_des_champions-1969.htm\" class=\"btn btn-primary\" 	role=\"button\">En savoir plus</a></p>
						      </div>
						    </div>
						  </div>
						
						<div class=\"col-sm-6 col-md-4 \">
						    <div class=\"thumbnail\">
						      <img src=\"source/RaymondMin.png\" alt=\"Raymond\" class=\"img-circle\">
						      <div class=\"caption\">
							<h3>JDE : pour Domenech, il faut croire en Lyon</h3>
							<p>C'est un fait assez rare : Coach Ray a décidé de faire confiance à Lyon, son club de cœur.Fin de la trêve internationale, la Ligue 1 reprend ses droits ! Raymond Domenech croit en Lyon, Toulouse et Marseille</p>
							<p> <a href=\"https://www.winamax.fr/news_jde-pour-domenech-il-faut-croire-en-lyon-29524\" class=\"btn btn-primary\" 									role=\"button\">En savoir plus</a></p>
						      </div>
						    </div>
						  </div>
						</div>
				</section>
			
	";
    }


}