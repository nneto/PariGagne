<?php
/**
 * Created by PhpStorm.
 * User: nacer
 * Date: 22/12/2016
 * Time: 17:34
 */
require_once('modele_AjoutEquipe.php');
require_once('vue_AjoutEquipe.php');

class ControleurAdminAjoutEquipe extends ControleurGenerique
{

    /**
     * ControleurInscription constructor.
     */
    public function __construct(){
        $this->modele=new ModelAjoutAdmin();
        $this->vue=new VueAjoutAdmin();

    }

    public function controleur_initForm(){
        $token=$this->modele->createToken();
        $this->vue->vue_afficheForm($token);
    }

    public function controleur_ajoutEquipe($tab){
        $r=$this->modele->getToken($tab['token']);
        if(empty($r)){
            $this->vue->vue_erreur("formulaire expirer");
        }else{
            $this->modele->effacerToken($tab['token']);
            $this->modele->addEquipe($tab);
            header('Location:index.php?module=accueil');
        }
    }
}