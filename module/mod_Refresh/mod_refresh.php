<?php

/**
 * Created by PhpStorm.
 * User: max
 * Date: 04/12/2016
 * Time: 19:21
 */
include_once('controleur_refresh.php');
class ModRefresh extends ModuleGenerique
{

    /**
     * ModRefresh constructor.
     */
    public function __construct()
    {
        $this->controleur= new ControleurRefresh();
    }
    public function refresh(){
        $this->controleur->refresh();
    }
}