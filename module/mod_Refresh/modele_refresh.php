<?php

/**
 * Created by PhpStorm.
 * User: max
 * Date: 04/12/2016
 * Time: 19:26
 */
class ModelRefresh extends ModeleGenerique
{
    public function getSolde($idCompte){
        $requeteSolde="select solde from compte where idCompte=?";
        $result=self::$connexion->prepare($requeteSolde);
        $result->execute(array($idCompte));
        $t=$result->fetchall(PDO::FETCH_COLUMN);

        return $t[0];
    }

}