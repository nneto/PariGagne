<?php

/**
 * Created by PhpStorm.
 * User: max
 * Date: 04/12/2016
 * Time: 19:24
 */
include_once('modele_refresh.php');
class ControleurRefresh extends ControleurGenerique
{


    /**
     * ControleurRefresh constructor.
     */
    public function __construct()
    {
        $this->modele= new ModelRefresh();

    }

    public function refresh(){
        $solde=$this->modele->getSolde($_SESSION['idcompte']);
        $_SESSION['solde']=$solde;
    }
}