<?php

/**
 * Created by PhpStorm.
 * User: nneto
 * Date: 18/11/16
 * Time: 11:22
 */
include_once('controleur_matches.php');
class ModMatches extends  ModuleGenerique
{
    public function module_matches(){
        $this->controleur=new ControleurMatches();
        $this->controleur-> listeMatches();
    }

    public function module_detail_matche($idmatch){
        $this->controleur=new ControleurMatches();
        $this->controleur-> detailsMatche($idmatch);
    }

    public function module_matches_recherche($recherche){
       $r= htmlspecialchars($recherche);
        $recherche = preg_replace('/\s{2,}/', ' ', trim($r));
        $this->controleur=new ControleurMatches();
        $this->controleur-> listeRechercheMatches($recherche);
    }

    public function module_matches_sport($idSport){
        $this->controleur=new ControleurMatches();
        $this->controleur->listeSportMatch($idSport);
    }

}