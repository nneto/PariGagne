<?php

/**
 * Created by PhpStorm.
 * User: nneto
 * Date: 18/11/16
 * Time: 11:23
 */
class ModelMatches extends  ModeleGenerique
{
    public function getMatches(){

        $requeteMatches=" select * from matchs where gagnant is NULL and dateMatch>=curdate() and idMatch NOT IN (Select idMatch from matchs where DateMatch=CurDate() and TIMEDIFF(heureMatch,Curtime())<='02:00:00') order by dateMatch ;";
        $result=self::$connexion->query($requeteMatches);
        $t=$result->fetchall(PDO::FETCH_ASSOC);

        return $t;
        /*
               $requeteMatches="select  matchs.gagnant as gagnant, participe.idmatch as idmatch ,participe.cote  as team1_cote,participe2.cote as team2_cote,participe3.cote as team3_cote ,equipe.nomTeam as nomTeam1,equipe1.nomTeam as nomTeam2,equipe2.nomTeam as nomTeam3,matchs.dateMatch as dateMatch,matchs.heureMatch as heureMatch,matchs.nomMatch as nomMatch
       from participe
           left join participe as participe2 on participe.idmatch=participe2.idmatch
           left join participe as participe3 on participe2.idmatch=participe3.idmatch
           inner join matchs on participe.idmatch=matchs.idmatch inner join equipe on participe.idteam = equipe.idteam
            INNER  join equipe as equipe1 on participe2.idteam = equipe1.idteam
            INNER JOIN equipe AS equipe2 on participe3.idteam = equipe2.idteam
        where participe.idteam<>participe2.idteam and
           participe.idteam <> participe3.idteam and
           participe2.idteam<>participe3.idteam and matchs.gagnant is NULL
           group by participe.idmatch ORDER BY dateMatch,heureMatch,participe.idteam;
       ";
       */
    }

    public function getParticipants(){
     $requetePartcipant="select * from equipe natural join participe order by idTeam desc;";
        $result=self::$connexion->query($requetePartcipant);
        $t=$result->fetchall(PDO::FETCH_ASSOC);
        return $t;
    }

    public function getSportMatch($idmatch){
        $requeteSportMatch="select nomSport,idSport from sport natural join matchs  where idmatch=$idmatch   ";
        $result=self::$connexion->query($requeteSportMatch);
        return $result->fetchall(PDO::FETCH_ASSOC);
    }
    public function getidMatches(){
        $requeteIdMatch="select distinct idmatch from matchs natural join participe where gagnant is null";
        $result=self::$connexion->query($requeteIdMatch);
        $t=$result->fetchall(PDO::FETCH_ASSOC);
        return $t;
    }
    public function getSports(){
        $requeteSports="select * from sport  ORDER by idSport Limit 5 ";
        $result=self::$connexion->query($requeteSports);
        return $result->fetchall(PDO::FETCH_ASSOC);
    }

    public function getDetailsMatchs($idMatche){
        $requeteDetailMatch="select * from matchs where idmatch=?";
        $result=self::$connexion->prepare($requeteDetailMatch);
        $result->execute(array($idMatche));
        return $result->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getParticipantMatch($idMatch){
        $requeteparticipant="select * from participe natural join equipe where idMatch=? order by idTeam desc";
        $result=self::$connexion->prepare($requeteparticipant);
        $result->execute(array($idMatch));
        return $result->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getSportMatchRecherche($idMatch){
        $requete="select  from matchs natural join sport where idMatch=? GROUP BY idSport";
        $result=self::$connexion->prepare($requete);
        $result->execute(array($idMatch));
        return $result->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getMatchesRecherche($r){
        $requete="select * from matchs NATURAL join participe natural join equipe where nomMatch like ? or nomTeam like ? and gagnant is null group by idmatch";
        $result = self::$connexion->prepare($requete);
        $result->execute(array('%'.$r.'%','%'.$r.'%'));
        return $result->fetchall(PDO::FETCH_ASSOC);
    }

    public function getidMatchesRecherche($r){
        $requete="select idMatch from matchs where nomMatch like ? and gagnant is null";
        $result = self::$connexion->prepare($requete);
        $result->execute(array('%'.$r.'%'));
        return $result->fetchall(PDO::FETCH_ASSOC);
    }
    public function getSport($id){
        $requeteSports="select * from sport  where idSport=$id  ";
        $result=self::$connexion->query($requeteSports);
        return $result->fetchall(PDO::FETCH_ASSOC);
    }

    public function getMatchesSport($idSport){
        $requete="select * from matchs where idSport=? and gagnant is null";
        $result = self::$connexion->prepare($requete);
        $result->execute(array($idSport));
        return $result->fetchall(PDO::FETCH_ASSOC);
    }
}