<?php

/**
 * Created by PhpStorm.
 * User: nneto
 * Date: 18/11/16
 * Time: 11:23
 */
class VueMatches extends VueGenerique
{
    public function afficheMatches($matches,$participe,$idMatches,$sports,$sportMatches){
        $cpt=0;
        $this->titre="matches";
        $this->Css=array("<link href=\"module/mod_Matches/matchs.css\" rel=\"stylesheet\" type=\"text/css\">");
        $this->contenu.="	<article>
			<div class=\"row\" id=\"main\">
				<h2  id=\"titre\"> Matchs en cours </h2>
				<hr/>
					<div class=\"col-md-2\" >
						<div class=\"list-group\">
							<a href=\"#\" class=\"list-group-item disabled\">
					    		    Catégories 	</a>";
        foreach ($sports as $sport){
            $nomSport=htmlspecialchars($sport['nomSport']);
            $this->contenu.= " <a href='index.php?module=matchs&idsport=$sport[idSport]' class='list-group-item'>$nomSport</a>";
        }
        $this->contenu.= "</div>
					</div>
					<div   class=\"col-md-6\">";
        foreach ($sportMatches as $sportMatch){
            $nomSport=htmlspecialchars($sportMatch[0]['nomSport']);
            $this->contenu.= " 
						<div class=\"panel panel-default\">
						  	<div class=\"panel-heading\">
						    		<h3 class=\"panel-title\">
						    		 $nomSport
						    		</h3>	
						    </div>";
            foreach($matches as $match){
                foreach ($idMatches as $idMatch){
                    if($match['idmatch'] == $idMatch['idmatch'] && $match['idSport'] == $sportMatch[0]['idSport']){
                        $nomMatch=htmlspecialchars($match['nomMatch']);
                        $dateMatch=htmlspecialchars($match['dateMatch']);
                        $heureMatch=$match['heureMatch'];

                        $this->contenu .= "<div class=\"panel-body list-group - item - text\">                       
                        ";
                        if((isset($_SESSION)&&isset($_SESSION['idcompte'])&&isset($_SESSION['login'])&&isset($_SESSION['solde'])&&isset($_SESSION['admin']))) {
                            if ($_SESSION['admin'] == 1) {
                                $this->contenu .= " 
  <a href='index.php?module=ModifMatch&id=$match[idmatch]'><h4 >$nomMatch</h4></a>
                                                    <button  style='width:6%;height:4%;font-size:80%;padding-left:2%;'class='btn btn-danger pull-right'  onclick='Affichesupprimer($match[idmatch])' id ='supprimer'>X</a></button>
						    		      		  <p >$dateMatch $heureMatch</p> ";
                            }else{
                                $this->contenu .="  		
						 <a href='index.php?module=matchs&idmatche=$match[idmatch]'><h4 class=\"list-group-item-heading\">$nomMatch</h4></a>
						    		      		  <p >$dateMatch $heureMatch</p>";
                            }
                        }else{
                            $this->contenu .="  		
						 <a href='index.php?module=matchs&idmatche=$match[idmatch]'><h4 class=\"list-group-item-heading\">$nomMatch</h4></a>
						    		      		   <p >$dateMatch $heureMatch</p>";
                        }
                        foreach ($participe as $particpant){
                            if($idMatch['idmatch']==$particpant['idmatch'] && $cpt<=5 ){
                                $nomTeam=htmlspecialchars($particpant['nomTeam']);
                                $cote=htmlspecialchars($particpant['cote']);
                                $this->contenu .= "	
									<span class=\"label label-default\">$nomTeam : $cote</span>		  
						  		";
                                $cpt++;
                            }
                        }$cpt=0;
                        $this->contenu.="</div>";
                    }
                }
            }
            $this->contenu.="</div>";
        }
        $this->contenu.= "
					</div>
					<div class=\"col-md-4\">
					<div class=\"row\">
								<h4 class=\"pull-right\">Rechercher un match</h4>
								<br>
								<hr/>
								<form method='post' action='index.php?module=matchs' class=\"navbar-form navbar-right inline-form  row\">
					      				<div class=\"form-group\">
										<input name='recherche' type=\"search\" class=\"input-sm f orm-control\" placeholder=\"Recherche\">
										<button type=\"submit\" class=\"btn btn-primary btn-sm\"><span class=\"glyphicon glyphicon-eye-open\"></span> Chercher</button>
					     				</div>
					    			</form>
						</div>";
        if(!(isset($_SESSION)&&isset($_SESSION['idcompte'])&&isset($_SESSION['login'])&&isset($_SESSION['solde']))){
            $this->contenu.= " <div class=\"aside\">
						<div class=\"row\" >
							<h4 class=\"pull-right\">Si vous etes nouveau</h4>
							<br>
							<hr/>
							<p class=\"pull-right\">Ne tardez pas,venez vous inscrire et bénéficier de 50Euros gratuit.</p>
						        <p class=\"pull-right\"><a class=\"btn btn-primary\" href=\"#\" role=\"button\">Inscrivez vous</a></p>

						</div>
						</div>
						<br>";
        }
        $this->contenu.= "	<div class=\"row\" class=\"aside\">
							<h4 class=\"pull-right\">Toutes les infos pour mieux parier</h4>
							<br>
							<hr/>
							<div class=\"row\">
						        <p class=\"pull-right\"><a class=\"btn btn-default\" href=\"#\" role=\"button\">Statistiques</a></p>
							</div>
							<div class=\"row\">
							<p class=\"pull-right\"><a class=\"btn btn-default\" href=\"#\" role=\"button\">En savoir plus sur les paris</a></p>
							</div>
						</div>
						<br>
						";


        if((isset($_SESSION)&&isset($_SESSION['idcompte'])&&isset($_SESSION['login'])&&isset($_SESSION['solde'])&&isset($_SESSION['admin']))) {
            if ($_SESSION['admin'] == 1) {
                $this->contenu.="
        <form method='post' action='index.php?module=ajoutMatch' >
                            		<p class=\"text-center\"><button type=\"submit\" class=\"btn btn-primary btn-sm \" >ajouter un match</button></p>
                           </form>
                           <form method='post' action='index.php?module=AjoutEquipe' style='margin-left:30%;' >
                            			<button type=\"submit\" class=\"btn btn-primary btn-sm \" >ajouter une Equipe</button>
                            			</form>
                            			<form method='post' action='index.php?module=AjoutSport' style='margin-left:30%;' >
                            			<button type=\"submit\" class=\"btn btn-primary btn-sm \" >ajouter un Sport</button>
                            			</form>
                            			";
            }
        }
        $this->contenu.= " 		</div>					
		</article>
		<script src='module\mod_Matches\bootbox.min.js' type='text/javascript'></script>
		<script src='module\mod_Matches\supprimer.js' type='text/javascript'></script>
		 ";
    }
    public function detailsMatch($detailMatch,$participants,$token){
        $this->Css=array("<link href='module/mod_Matches/parier.js'>" , "<link href='module/mod_Matches/bootbox.min.js'>","<link href='module/mod_Matches/details-matchs.css' rel='stylesheet'type='text/css'> ");
        $d=date("d-m-Y", strtotime($detailMatch['dateMatch']));
        $this->titre.=htmlspecialchars($detailMatch['nomMatch']);
        $nomMatch=htmlspecialchars($detailMatch['nomMatch']);
        $heureMatch=htmlspecialchars($detailMatch['heureMatch']);
        $this->contenu.="            
			<div class=\"row \" id=\"main\">
				<h2  id=\"titre\"> $nomMatch</h2>
				<hr/>
					<div class=\"col-md-8\" id='centre'>
						<div class=\" row text-center\">
						    <h3>$d</h3> <h3>$heureMatch</h3>
						</div>
						<br>
						<div class=\"row\" >
							<div class=\"panel panel-default\">
						  		<div class=\"panel-heading\">
						    			<h3 class=\"panel-title\">Résultat du match</h3>
						  		</div>
						 		<div class=\"panel-body\">
						   		";
        foreach ($participants as $participant){
            $nomTeam=htmlspecialchars($participant['nomTeam']);
            if($participant['nomTeam']=='NUL'){
                $this->contenu.=" <p class=\"equipe\"> $nomTeam<span class=\"badge\">$participant[cote]</span> </p>";
            }else{
                $this->contenu.=" <p class=\"equipe\" ><img class='logo' alt=''src='source/$participant[logo]' > $nomTeam<span class=\"badge\">$participant[cote]</span> </p>";
            }
        }
        $this->contenu.= "
                    </div>
                     </div>";
        if((isset($_SESSION)&&isset($_SESSION['idcompte'])&&isset($_SESSION['login'])&&isset($_SESSION['solde'])&&isset($_SESSION['admin']))){
            if($_SESSION['admin']==0){
                $this->contenu.= "            <button  class=\"btn btn-primary pull-right\"  onclick='afficher($_GET[idmatche],$_SESSION[idcompte],\"$token\")' id ='parie'> Parier</a></button>
                   ";
            }
        }
        $this->contenu.="             </div>
					</div>
		
					<div class=\"col-md-4\" id='droite'>
					<div class=\"row\">
								<h4 class=\"pull-right\">Rechercher un match</h4>
								<br>
								<hr/>
								<form method='post' action='index.php?module=matchs' class=\"navbar-form navbar-right inline-form  row\">
					      				<div class=\"form-group\">
										<input type=\"search\" name='recherche' class=\"input-sm form-control\" placeholder=\"Recherche\">
										<button type=\"submit\" class=\"btn btn-primary btn-sm\"><span class=\"glyphicon glyphicon-eye-open\"></span> Chercher</button>
					     				</div>
					    			</form>
						</div>";

        if(! isset($_SESSION)&&isset($_SESSION['idcompte'])&&isset($_SESSION['login'])&&isset($_SESSION['solde']) && isset($_SESSION['admin'])) {
            if($_SESSION['admin']==0){

                $this->contenu.= " <div class=\"aside\">
						<div class=\"row\" >
							<h4 class=\"pull-right\">Si vous etes nouveau</h4>
							<br>
							<hr/>
							<p class=\"pull-right\">Ne tardez pas,venez vous inscrire et bénéficier de 50Euros gratuit.</p>
						        <p class=\"pull-right\"><a class=\"btn btn-primary\" href=\"#\" role=\"button\">Inscrivez vous</a></p>

						</div>
						</div>
						<br>";
            }
        }
        $this->contenu.= "	<div class=\"row\" class=\"aside\">
							<h4 class=\"pull-right\">Toutes les infos pour mieux parier</h4>
							<br>
							<hr/>
							<div class=\"row\">
						        <p class=\"pull-right\"><a class=\"btn btn-default\" href=\"#\" role=\"button\">Statistiques</a></p>
							</div>
							<div class=\"row\">
							<p class=\"pull-right\"><a class=\"btn btn-default\" href=\"#\" role=\"button\">En savoir plus sur les paris</a></p>
							</div>
						</div>
					</div>
		</article>
			<script src='module\mod_Matches\parier.js' type='text/javascript'></script>
			<script src='module\mod_Matches\bootbox.min.js' type='text/javascript'></script>";
    }
}
