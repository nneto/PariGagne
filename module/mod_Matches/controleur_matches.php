<?php

/**
 * Created by PhpStorm.
 * User: nneto
 * Date: 18/11/16
 * Time: 11:23
 */
include_once('modele_matches.php');
include_once('vue_matches.php');
class ControleurMatches extends ControleurGenerique
{   /**
 * ControleurMatches constructor.
 */
    public function __construct()
    {
        $this->modele = new ModelMatches();
        $this->vue = new VueMatches();
    }
    public function listeMatches(){
        $matches=$this->modele->getMatches();
        $participants=$this->modele->getParticipants();
        $idMatches=$this->modele->getidMatches();
        $sports=$this->modele->getSports();
        $idsport=array();
        $sportMatch=array();
        foreach ($matches as $matche) {
            array_push($idsport, $this->modele->getSportMatch($matche['idmatch'])[0]['idSport']);
        }
        $idsport=array_unique($idsport);
        foreach ($idsport as $id){
            array_push($sportMatch,$this->modele->getSport($id));
        }

        if(empty($matches) || empty($participants) || empty($idMatches)){
            $this->vue->vue_erreur("aucun matches a afficher");
        }else{
            $this->vue->afficheMatches($matches, $participants,$idMatches,$sports,$sportMatch);
        }
    }

    public function detailsMatche($idMatch){
        $token=$this->modele->createToken();
        $id=htmlspecialchars($idMatch);
        $detailsMatch=$this->modele->getDetailsMatchs($id);
        $participant=$this->modele->getParticipantMatch($idMatch);
        if($detailsMatch==null || $participant==null){
            $this->vue->vue_erreur("404 Page NOT FOUND");
        }else{
            $this->vue->detailsMatch($detailsMatch[0],$participant,$token);
        }
    }

    public function listeRechercheMatches($r)
    {
        $matchs = $this->modele->getMatchesRecherche($r);
        $participants = $this->modele->getParticipants();
        $idMatches = $this->modele->getidMatches();
        $sports = $this->modele->getSports();
        $idsport = array();
        $sportMatch = array();
        foreach ($matchs as $matche) {
            array_push($idsport, $this->modele->getSportMatch($matche['idmatch'])[0]['idSport']);
        }
        $idsport = array_unique($idsport);
        foreach ($idsport as $id) {
            array_push($sportMatch, $this->modele->getSport($id));
        }
        if (empty($matchs) || empty($participants) || empty($idMatches)) {
            $this->vue->vue_erreur("aucun matches a afficher");
        } else {
            $this->vue->afficheMatches($matchs, $participants, $idMatches, $sports, $sportMatch);
        }
    }

    public function listeSportMatch($idSport){
        $matchs = $this->modele->getMatchesSport($idSport);
        $participants = $this->modele->getParticipants();
        $idMatches = $this->modele->getidMatches();
        $sports = $this->modele->getSports();
        $idsport = array();
        $sportMatch = array();

        foreach ($matchs as $matche) {
            array_push($idsport, $this->modele->getSportMatch($matche['idmatch'])[0]['idSport']);
        }
        $idsport = array_unique($idsport);
        foreach ($idsport as $id) {
            array_push($sportMatch, $this->modele->getSport($id));
        }
        if (empty($matchs) || empty($participants) || empty($idMatches)) {
            $this->vue->vue_erreur("aucun matches a afficher");
        } else {
            $this->vue->afficheMatches($matchs, $participants, $idMatches, $sports, $sportMatch);
        }
    }
}
