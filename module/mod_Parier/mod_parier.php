<?php

/**
 * Created by PhpStorm.
 * User: max
 * Date: 24/11/2016
 * Time: 21:18
 */
include_once('controleur_parier.php');
class ModParier extends ModuleGenerique
{
    /**
     * ModParier constructor.
     */
    public function __construct()
    {
        $this->controleur= new ControleurParier();
    }

    public function moduleFormParier($idMatch){
        $this->controleur->controleurFormParie($idMatch);
    }

    public function moduleParieEffectuer($idEquipe,$typeParie,$mise,$idMatch,$token,$idParie=0){
        $this->controleur->controleurParieEffectuer($idEquipe,$typeParie,$mise,$idMatch,$token,$idParie);

    }
}