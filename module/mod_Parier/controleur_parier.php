<?php

/**
 * Created by PhpStorm.
 * User: max
 * Date: 24/11/2016
 * Time: 21:19
 */
include_once('modele_parier.php');
include_once('vue_parier.php');
class ControleurParier extends ControleurGenerique
{
    /**
     * ControleurParier constructor.
     */
    public function __construct()
    {
        $this->modele= new ModelParier();
        $this->vue= new VueParier();
    }

    public function controleurFormParie($idMatch){
        $idmatch=htmlspecialchars($idMatch);
        $participants=$this->modele->getPartcipant($idmatch);
        $detailsMatch=$this->modele->getDetailsMatchs($idmatch);
        $this->vue->vue_formParier($idmatch,$detailsMatch,$participants);
    }

    public function controleurParieEffectuer($idEquipe,$typeParie,$mise,$idMatche,$token,$idParie){
        $idTeam=htmlspecialchars($idEquipe);
        $type=htmlspecialchars($typeParie);
        $montant=htmlspecialchars($mise);
        $idMatch=htmlspecialchars($idMatche);
        $idParie=htmlspecialchars($idParie);
        $t=$this->modele->getToken($token);
        if(empty($t)){
            $this->modele->effacerToken($token);
            $this->vue->vue_erreur("erreur token");
        }else{
            $this->modele->effacerToken($token);
            if($mise>$_SESSION['solde']){
                $this->vue->vue_erreur("Solde insuffisant");
            }else{
                if($type=='simple'){

                    if(empty($this->modele->getParieExistant($idMatch,$_SESSION['idcompte']))){

                        if($this->modele->parier($idMatch,$idTeam,$montant,$_SESSION['idcompte'])){
                            $this->modele->setSolde($montant,$_SESSION['idcompte']);
                            header('Location:index.php?module=accueil');
                            //           $detailParie=$this->modele->getDetailParie();
                            //        $this->vue=confirmParie($detailParie);

                        }else{
                            $this->vue->vue_erreur("parie Impossible ou parie déjà existant");
                        }
                    }else{
                        $this->vue->vue_erreur("Vous avez déjà parier pour ce match");
                    }
                }else{
                    if($idParie==0){
                        if(empty($this->modele->getParieExistant($idMatch,$_SESSION['idcompte']))){
                            if($this->modele->parier($idMatch,$idTeam,$montant,$_SESSION['idcompte'])){
                                $this->modele->setSolde($montant,$_SESSION['idcompte']);

                                //           $detailParie=$this->modele->getDetailParie();
                                //        $this->vue=confirmParie($detailParie);

                            }else{
                                $this->vue->vue_erreur("parie Impossible parie existe 2");
                            }
                        }else{
                            $this->vue->vue_erreur("Vous avez déjà parier pour ce match");
                        }
                    }else{
                        if(empty($this->modele->getParieExistant($idMatch,$_SESSION['idcompte']))){
                            if($this->modele->parieMultiple($idMatch,$idTeam,$montant,$_SESSION['idcompte'],$idParie)){
                                $this->modele->setSolde($montant,$_SESSION['idcompte']);
                            }else{
                                $this->vue->vue_erreur("parie Impossible parieexiste 3");
                            }
                        }else{
                            $this->vue->vue_erreur("parie existant");
                        }
                    }
                }
            }
        }

    }
}