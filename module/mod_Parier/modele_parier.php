<?php

/**
 * Created by PhpStorm.
 * User: max
 * Date: 24/11/2016
 * Time: 21:22
 */


class ModelParier extends ModeleGenerique
{

    public function getPartcipant($idMatch){
        $requeteParticpant='select * from participe NATURAL JOIN equipe where idmatch=?';
        $result=self::$connexion->prepare($requeteParticpant);
        $result->execute(array($idMatch));
        return $result->fetchall(PDO::FETCH_ASSOC);
    }

    public function getDetailsMatchs($idMatche){
        $requeteDetailMatch="select * from matchs where idmatch=?";
        $result=self::$connexion->prepare($requeteDetailMatch);
        $result->execute(array($idMatche));
        $t= $result->fetchAll(PDO::FETCH_ASSOC);
        return $t[0];
    }

    public function parier($idMatch,$idTeam,$montant,$idCompte){
        $requeteParie="INSERT INTO `parie`(`idParie`, `mise`, `dateParie`, `heureParie`,  `idCompte`, `idmatch`, `idTeam`) VALUES (idPlusParier(),?,CurDate(),CurTime(),?,?,?);";
        $result=self::$connexion->prepare($requeteParie);
        return $result->execute(array($montant,$idCompte,$idMatch,$idTeam));
    }

    public function parieMultiple($idMatch,$idTeam,$montant,$idCompte,$idParie){
        $requeteParie="INSERT INTO `parie`(`idParie`, `mise`, `dateParie`, `heureParie`, `gagne`, `idCompte`, `idmatch`, `idTeam`) VALUES (?,?,CurDate(),CurTime(),NULL,?,?,?);";
        $result=self::$connexion->prepare($requeteParie);
        return $result->execute(array($idParie,$montant,$idCompte,$idMatch,$idTeam));
    }

    public function setSolde($mise,$idCompte){
        $requeteSolde='Update compte set Solde=Solde-? where idcompte=?';
        $result=self::$connexion->prepare($requeteSolde);
        $result->execute(array($mise,$idCompte));
    }

    public function getDetails($idParie){
        $requeteDetail='select * from parie natural join match natural join equipe where idParie=?';
        $result=self::$connexion->prepare($requeteDetail);
        $result->execute(array($idParie));
        $t=$result->fetchAll(PDO::FETCH_ASSOC);
        return $t[0];
    }
    public function getParieExistant($idMatch,$idCompte){
        $requete="select * from parie where idmatch=? and idCompte=? ";
        $result = self::$connexion->prepare($requete);
        return $result->fetchAll(PDO::FETCH_ASSOC);
    }
}