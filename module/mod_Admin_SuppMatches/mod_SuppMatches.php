<?php
/**
 * Created by PhpStorm.
 * User: nacer
 * Date: 06/01/2017
 * Time: 11:53
 */
include_once('Controleur_SuppMatches.php');
    class ModSuppAdmin extends ModuleGenerique
{
    public function module_SuppAdmin(){
        $this->controleur=new ControleurAdminSuppMatches();
        $this->controleur->controleur_initForm();
    }
    public function module_SuppMatches($id){

        $id=htmlspecialchars($id);
        $this->controleur=new ControleurAdminSuppMatches();
        $this->controleur->controler_adminMatch($id);
    }


}