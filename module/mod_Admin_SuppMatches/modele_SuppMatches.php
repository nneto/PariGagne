<?php
/**
 * Created by PhpStorm.
 * User: nacer
 * Date: 06/01/2017
 * Time: 11:54
 */
class ModelSuppAdmin extends ModeleGenerique{
    public function suppMatches($id){
       $m= $this->getMatch($id);

       if (empty($m)){
           return "erreur";
       }else{
           $this->remettreSolde($id);
           $this->deleteParie($id);
           $this->deleteParticipant($id);
           $requeteAjout="delete from matchs where idmatch=?";
           $requete=self::$connexion->prepare($requeteAjout);
           $result=$requete->execute(array($id));
           return "Suppression effectuer";
       }



    }

    public function getMatch($id){
        $requeteAjout="select *  from matchs where idmatch=?";
        $requete=self::$connexion->prepare($requeteAjout);
        $requete->execute(array($id));

        $t=$requete->fetchAll(PDO::FETCH_ASSOC);
        return $t;
    }
    public function deleteParie($id){
        $requeteAjout="delete from parie where idmatch=?";
        $requete=self::$connexion->prepare($requeteAjout);
        $result=$requete->execute(array($id));

    }

    public function remettreSolde($id){
        $requeteAjout="select idCompte,mise from parie where idmatch=?";
        $requete=self::$connexion->prepare($requeteAjout);
        $requete->execute(array($id));
        $t= $requete->fetchAll(PDO::FETCH_ASSOC);
        $requeteAjout="update compte set solde=solde  + ? where idcompte=?";
        $requete=self::$connexion->prepare($requeteAjout);
        foreach ($t as $parie){

            $requete->execute(array($parie['mise'],$parie['idCompte']));
        }
    }

    public function deleteParticipant($id){
        $requeteAjout="delete from participe where idmatch=?";
        $requete=self::$connexion->prepare($requeteAjout);
        $result=$requete->execute(array($id));

    }

}