<?php
/**
 * Created by PhpStorm.
 * User: nacer
 * Date: 06/01/2017
 * Time: 12:17
 */
require_once('modele_SuppMatches.php');
require_once('vue_SuppMatches.php');

class ControleurAdminSuppMatches extends ControleurGenerique{

    public function __construct(){
        $this->vue=new VueSuppAdmin();
        $this->modele=new ModelSuppAdmin();
    }

    public function controleur_initForm(){
        $this->vue->vue_afficheForm();
    }

    public function controler_adminMatch($id){
        $this->modele->suppMatches($id);
        header('Location:index.php?module=matchs');
    }
}