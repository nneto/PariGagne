<?php
/**
 * Created by PhpStorm.
 * User: nacer
 * Date: 22/12/2016
 * Time: 17:33
 */
include_once('controleur_AjoutSport.php');
class ModAjoutAdmin extends ModuleGenerique{

    public function module_AjoutAdmin(){
        $this->controleur=new ControleurAdminAjoutSport();
        $this->controleur->controleur_initForm();
    }
    public function module_AjoutEquipe($tab){
    	$this->controleur = new ControleurAdminAjoutSport();
    	$this->controleur->controleur_ajoutEquipe($tab);
    }


}