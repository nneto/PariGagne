<?php

/**
 * Created by PhpStorm.
 * User: NICO
 * Date: 23/01/2017
 * Time: 14:02
 */
class ModelModifProfil extends ModeleGenerique
{
    public function getMdp($idcompte,$psw){
        $crypt=$this->mdpCrypt($psw,$_SESSION['login']);
        $requete= "select * from compte where idcompte=? and motDePasse=?";
        $result=self::$connexion->prepare($requete);
        $result->execute(array($idcompte,$crypt));
        return $result->fetchall(PDO::FETCH_ASSOC);

    }

    public function getPseudo($pseudo){
        $requete= "select * from compte where pseudo=?";
        $result=self::$connexion->prepare($requete);
        $result->execute(array($pseudo));
        return $result->fetchall(PDO::FETCH_ASSOC);
    }
    public function changerMdp($idcompte,$psw){
        $crypt=$this->mdpCrypt($psw,$_SESSION['login']);
        $requete= "update compte set motDePasse=? where idcompte=?";
        $result=self::$connexion->prepare($requete);
        $result->execute(array($crypt,$idcompte));
    }

    public function changerPseudo($idcompte,$pseudo){
        $requete= "update compte set pseudo=? where idcompte=?";
        $result=self::$connexion->prepare($requete);
        $result->execute(array($pseudo,$idcompte));
    }
    public function getEmail($email){
        $requete= "select * from compte where email=?";
        $result=self::$connexion->prepare($requete);
        $result->execute(array($email));
        return $result->fetchall(PDO::FETCH_ASSOC);
    }

    public function changerEmail($idcompte,$email){
        $requete= "update compte set email=? where idcompte=?";
        $result=self::$connexion->prepare($requete);
        $result->execute(array($email,$idcompte));
    }
}