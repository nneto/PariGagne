/**
 * Created by NICO on 23/01/2017.
 */


var inputPseudo=document.getElementById('pseudo');
var  pseudoCorrect = false ;
inputPseudo.addEventListener("keyup",function(){
        var tips=document.getElementById('pseudoTips');
        var value = inputPseudo.value;
        if((value.length<5) || (value.length>20)){
            inputPseudo.style.borderColor="red";
            tips.style.visibility='visible';
            tips.innerHTML="Doit être compris entre 5 et 20 caractères";
            tips.style.color="red";
            pseudoCorrect=false;
        }else{
            inputPseudo.style.borderColor="green";
            tips.innerHTML="Saisie correct";
            tips.style.color="green";
            pseudoCorrect=true;
        }

    }
);

function verif() {
    return pseudoCorrect;
}