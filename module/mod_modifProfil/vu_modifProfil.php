<?php

/**
 * Created by PhpStorm.
 * User: NICO
 * Date: 23/01/2017
 * Time: 14:05
 */
class VueModifProfil extends VueGenerique
{
    public function afficheFormMdp($token){
        $this->titre="Modif Mdp";
        $this->contenu="
        <form style='width:40%;height:40%;margin-left:35%' method='post' action='index.php?module=modifMdp'>
        <input type='hidden' name='token' value='$token'><br>
        <a style='text-decoration:none;color:#0f0f0f'>Ancien mot de passe :</a><br>
        <input style='border-radius:5px;border: 1px solid black' type='password' name='oldpsw' id='oldpsw' required> </input><br>
        <a style='text-decoration:none;color:#0f0f0f'>Nouveau mot de passe :</a><br>
        <input style='border-radius:5px;border: 1px solid black' type='password' name='newpsw' id='newpsw' required> </input><br>
         <span  style=' visibility: hidden'   id=\"mdp1Tips\">Doit être compris entre 7 et 30 caractère</span><br>
        <a style='text-decoration:none;color:#0f0f0f'>Confirmer mot de passe :</a><br>
        <input style='border-radius:5px;border: 1px solid black' type='password' name='confirmpsw' id='confirmpsw' required> </input><br>     
         <span  style=' visibility: hidden'   id=\"mdp2Tips\">Doit être compris entre 7 et 30 caractère</span><br>
        <input style='margin-left:9%;margin-top:3%' class=\"btn btn-primary btn-sm \" type='submit' value='envoyer'> </input>   
        </form>
        <script src='module/mod_modifProfil/modifpsw.js'></script>
        ";
    }
    public function afficheFormPseudo($token){
        $this->titre="Modif Pseudo";

        $this->contenu="
        <form method='post' action='index.php?module=modifPseudo' onsubmit=\"return verif() \" >
        <input type='hidden' name='token' value='$token'>
        Nouveau Pseudo:
        <input type='text' name='pseudo' id='pseudo' required> </input>
         
        <input type='submit' value='envoyer'> </input>   
        <span  style=' visibility: hidden'   id=\"pseudoTips\">Doit être compris entre 5 et 20 caractère</span>
        </form>
        </article>
        <script src='module/mod_modifProfil/modifPseudo.js'></script>
        ";
    }

    public function afficheFormEmail($token){
        $this->titre="Modif Email";
        $this->Css=array("<script src='module/mod_modifProfil/modifEmail.js'></script>");
        $this->contenu="
        <form method='post' action='index.php?module=modifEmail'>
        <input type='hidden' name='token' value='$token'>
        Nouvelle addresse email:
        <input type='email' name='email' id='email' required> </input> 
        <input type='submit' value='envoyer'> </input>   
        </form>
        ";
    }

}