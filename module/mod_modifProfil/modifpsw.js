/**
 * Created by NICO on 23/01/2017.
 */
var inputMdp1=document.getElementById('newpsw');
var inputMdp2=document.getElementById('confirmpsw');
var motDePasseCorrect=false;

inputMdp1.addEventListener("keyup",function(){
    var tips=document.getElementById('mdp1Tips');
    var tips2=document.getElementById('mdp2Tips');
    var value = inputMdp1.value;

    if(value.length <7){
        inputMdp1.style.borderColor="red";
        tips.style.visibility='visible';
        tips.innerHTML="Doit être compris entre 7 et 30 caractère";
        motDePasseCorrect=false;
    }else{
        tips.style.visibility='hidden';
        inputMdp1.style.borderColor="green";
        tips.style.color="green";
        tips.innerHTML="Saisie correct";
        if(inputMdp1.value==inputMdp2.value){

            tips.style.color="green";
            inputMdp2.style.borderColor="green";
            tips.innerHTML="Saisie correct"

            tips2.style.color="green";
            inputMdp1.style.borderColor="green";
            tips2.innerHTML="Saisie correct"
            motDePasseCorrect=true;
        }else{
            tips.style.visibility='visible';
            tips.style.color="red";
            tips2.style.visibility='visible';
            tips2.style.color="red";
            inputMdp2.style.borderColor="red";
            inputMdp1.style.borderColor="red";
            tips.innerHTML="Les deux mot de passe doit être identique";
            tips2.innerHTML="Les deux mot de passe doit être identique";
            motDePasseCorrect=false;
        }
    }

});

inputMdp2.addEventListener("keyup",function(){
    var tips=document.getElementById('mdp2Tips');
    var tips2=document.getElementById('mdp1Tips');
    var value = inputMdp2.value;

    if(value.length <7){
        inputMdp2.style.borderColor="red";
        tips.style.visibility='visible';
        tips.innerHTML="Doit être compris entre 7 et 30 caractère";
        motDePasseCorrect=false;
    }else{
        if(inputMdp1.value==inputMdp2.value){

            tips.style.color="green";
            inputMdp2.style.borderColor="green";
            tips.innerHTML="Saisie correct"

            tips2.style.color="green";
            inputMdp1.style.borderColor="green";
            tips2.innerHTML="Saisie correct"
            motDePasseCorrect=true;
        }else{
            tips.style.visibility='visible';
            tips.style.color="red";
            tips2.style.visibility='visible';
            tips2.style.color="red";
            inputMdp2.style.borderColor="red";
            inputMdp1.style.borderColor="red";
            tips.innerHTML="Les deux mot de passe doit être identique";
            tips2.innerHTML="Les deux mot de passe doit être identique";
            motDePasseCorrect=false;
        }
    }

});


function verif() {
    return pseudoCorrect;
}