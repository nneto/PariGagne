<?php

/**
 * Created by PhpStorm.
 * User: NICO
 * Date: 23/01/2017
 * Time: 14:01
 */
include_once ('controleur_modifProfil.php');
class ModModifProfil extends ModuleGenerique

{
    public function __construct()
    {
        $this->controleur= new ControleurModifProfil();
    }

    public function moduleFormMdp(){
        $this->controleur->controleurFormMdp();
    }
    public function moduleFormPseudo(){
        $this->controleur->controleurFormPseudo();
    }
    public function moduleFormEmail(){
        $this->controleur->controleurFormEmail();
    }

    public function moduleChangeMdp($idCompte,$tab){
        $this->controleur->controleurChangeMdp($idCompte,$tab);
    }
    public function moduleChangePseudo($idCompte,$tab){
        $this->controleur->controleurChangePseudo($idCompte,$tab);
    }
    public function moduleChangeEmail($idCompte,$tab){
        $this->controleur->controleurChangeEmail($idCompte,$tab);
    }
}