<?php

/**
 * Created by PhpStorm.
 * User: NICO
 * Date: 23/01/2017
 * Time: 14:00
 */
include_once ('vu_modifProfil.php');
include_once ('modele_modifProfil.php');
class ControleurModifProfil extends ControleurGenerique
{

    public function __construct()
    {
        $this->vue= new VueModifProfil();
        $this->modele= new ModelModifProfil();
    }

    public function controleurFormMdp(){
        $token=$this->modele->createToken();
        $this->vue->afficheFormMdp($token);
    }

    public function controleurFormPseudo(){
        $token=$this->modele->createToken();
        $this->vue->afficheFormPseudo($token);
    }

    public function controleurFormEmail(){
        $token=$this->modele->createToken();
        $this->vue->afficheFormEmail($token);
    }

    public function controleurChangeMdp($idCompte,$tab){
        $t=$this->modele->getToken($tab['token']);
        if(empty($t)){
            $this->modele->effacerToken($tab['token']);
            $this->vue->vue_erreur("erreur token");
        }else {
            $this->modele->effacerToken($tab['token']);
            $old=$this->modele->getMdp($idCompte,$tab['oldpsw']);
            if (empty($old)) {
                $this->vue->vue_erreur("erreur mot de passe");
            }else{
                if ($tab['newpsw'] == $tab['confirmpsw']) {
                    $this->modele->changerMdp($idCompte, $tab['newpsw']);
                    header('Location:index.php?module=deconnexion');
                } else {
                    $this->vue->vue_erreur("erreur mot de passe");
                }
            }
        }
    }

    public function controleurChangePseudo($idCompte,$tab){
        $t=$this->modele->getToken($tab['token']);
        if(empty($t)){
            $this->modele->effacerToken($tab['token']);
            $this->vue->vue_erreur("erreur token");
        }else {
            $this->modele->effacerToken($tab['token']);
            $old=$this->modele->getPseudo($tab['pseudo']);
            if (empty($old)) {
                $this->modele->changerPseudo($idCompte,$tab['pseudo']);
                header('Location:index.php?module=deconnexion');
            }else{
                $this->vue->vue_erreur("pseudo déjà pris");
            }
        }
    }

    public function controleurChangeEmail($idCompte,$tab){
        $t=$this->modele->getToken($tab['token']);
        if(empty($t)){
            $this->modele->effacerToken($tab['token']);
            $this->vue->vue_erreur("erreur token");
        }else {
            $this->modele->effacerToken($tab['token']);
            $old=$this->modele->getEmail($tab['email']);
            if (empty($old)) {
                $this->modele->changerEmail($idCompte,$tab['email']);
                header('Location:index.php?module=deconnexion');
            }else{
                $this->vue->vue_erreur("email déjà utiliser");
            }
        }
    }
}