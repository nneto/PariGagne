<?php

/**
 * Created by PhpStorm.
 * User: max
 * Date: 21/11/2016
 * Time: 00:51
 */
include_once('modele_resultats.php');
include_once('vue_resultats.php');
class ControleuResultats extends ControleurGenerique
{
    public function resultats(){

        $this->modele=new ModelResultats();
        $this->vue=new VueResultats();
        $matches=$this->modele->getMatches();
        $participants=$this->modele->getParticipants();
        $idMatches=$this->modele->getidMatches();
        $sports=$this->modele->getSports();
        $idsport=array();
        $sportMatch=array();
        foreach ($matches as $matche) {
                array_push($idsport, $this->modele->getSportMatch($matche['idmatch'])['idSport']);

        }
        $idsport=array_unique($idsport);
        foreach ($idsport as $id){
            array_push($sportMatch,$this->modele->getSport($id));
        }
        if(empty($matches) || empty($participants) || empty($idMatches)){
            $this->vue->vue_erreur("aucun matches a afficher");
        }else{
            $this->vue->afficheResultats($matches, $participants,$idMatches,$sports,$sportMatch);
        }
    }

    public function detailsResultat($idMatch){
        $this->modele=new ModelResultats();
        $this->vue=new VueResultats();
        $id=htmlspecialchars($idMatch);
        $detailsMatch=$this->modele->getDetailsMatchs($id);
        $participant=$this->modele->getParticipantMatch($idMatch);

        $this->vue->detailsResultat($detailsMatch[0],$participant);

    }

    public function listeRechercheResultat($r)
    {

        $this->modele = new ModelResultats();
        $this->vue = new VueResultats();
        $matchs = $this->modele->getMatchesRecherche($r);

        $participants=$this->modele->getParticipants();
        $idMatches=$this->modele->getidMatches();
        $sports=$this->modele->getSports();
        $idsport=array();
        $sportMatch=array();
        foreach ($matchs as $matche) {
            array_push($idsport, $this->modele->getSportMatch($matche['idmatch'])['idSport']);

        }
        $idsport=array_unique($idsport);
        foreach ($idsport as $id){
            array_push($sportMatch,$this->modele->getSport($id));
        }

        if(empty($matchs) || empty($participants) || empty($idMatches)){
            $this->vue->vue_erreur("aucun matches a afficher");
        }else{
            $this->vue->afficheResultats($matchs, $participants,$idMatches,$sports,$sportMatch);
        }
    }

    public function listeSportMatch($idSport){
        $this->modele=new ModelResultats();
        $this->vue=new VueResultats();
        $matchs = $this->modele->getMatchesSport($idSport);
        $participants = $this->modele->getParticipants();
        $idMatches = $this->modele->getidMatches();
        $sports = $this->modele->getSports();
        $idsport = array();
        $sportMatch = array();

        foreach ($matchs as $matche) {
            array_push($idsport, $this->modele->getSportMatch($matche['idmatch'])['idSport']);
        }
        $idsport = array_unique($idsport);
        foreach ($idsport as $id) {
            array_push($sportMatch, $this->modele->getSport($id));
        }
        if (empty($matchs) || empty($participants) || empty($idMatches)) {
            $this->vue->vue_erreur("aucun matches a afficher");
        } else {
            $this->vue->afficheResultats($matchs, $participants,$idMatches,$sports,$sportMatch);
        }
    }
}