<?php

/**
 * Created by PhpStorm.
 * User: max
 * Date: 21/11/2016
 * Time: 00:55
 */
class ModelResultats extends ModeleGenerique
{
    public function getMatches(){
        /* $requeteMatches="select  participe.idmatch as idmatch ,participe.cote  as team1_cote,participe2.cote as team2_cote,participe3.cote as team3_cote ,equipe.nomTeam as nomTeam1,equipe1.nomTeam as nomTeam2,equipe2.nomTeam as nomTeam3,matchs.dateMatch as dateMatch,matchs.heureMatch as heureMatch,matchs.nomMatch as nomMatch
     from participe
     left join participe as participe2 on participe.idmatch=participe2.idmatch
     left join participe as participe3 on participe2.idmatch=participe3.idmatch
     inner join matchs on participe.idmatch=matchs.idmatch inner join equipe on participe.idteam = equipe.idteam
      INNER  join equipe as equipe1 on participe2.idteam = equipe1.idteam
      INNER JOIN equipe AS equipe2 on participe3.idteam = equipe2.idteam
      where participe.idteam<>participe2.idteam and
     participe.idteam <> participe3.idteam and
     participe2.idteam<>participe3.idteam and Gagnant is not NULL
     group by participe.idmatch ORDER BY dateMatch,heureMatch;
 ";*/
        $requeteMatches="select * from matchs where gagnant is not NULL order by dateMatch";
        $result=self::$connexion->query($requeteMatches);
        return $result->fetchall(PDO::FETCH_ASSOC);


    }

    public function getParticipants(){
        $requetePartcipant="select * from equipe natural join participe where idTeam<>0 order by idTeam desc;";
        $result=self::$connexion->query($requetePartcipant);
        $t=$result->fetchall(PDO::FETCH_ASSOC);
        return $t;
    }

    public function getSportMatch($idmatch){
        $requeteSportMatch="select nomSport,idSport from sport natural join matchs  where idmatch=$idmatch";
        $result=self::$connexion->query($requeteSportMatch);
        $t= $result->fetchall(PDO::FETCH_ASSOC);
        return $t[0];
    }
    public function getidMatches(){
        $requeteIdMatch="select distinct idmatch from matchs natural join participe where gagnant is not null ";
        $result=self::$connexion->query($requeteIdMatch);
        $t=$result->fetchall(PDO::FETCH_ASSOC);

        return $t;
    }
    public function getSports(){
        $requeteSports="select * from sport  ORDER by idSport Limit 5 ";
        $result=self::$connexion->query($requeteSports);
        return $result->fetchall(PDO::FETCH_ASSOC);
    }

    public function getSport($id){
        $requeteSports="select * from sport  where idSport=$id  ";
        $result=self::$connexion->query($requeteSports);
        return $result->fetchall(PDO::FETCH_ASSOC);
    }

    public function getDetailsMatchs($idMatche){
        $requeteDetailMatch="select * from matchs where idmatch=?";
        $result=self::$connexion->prepare($requeteDetailMatch);
        $result->execute(array($idMatche));
        return $result->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getParticipantMatch($idMatch){
        $requeteparticipant="select * from participe natural join equipe where idMatch=? order by idTeam desc";
        $result=self::$connexion->prepare($requeteparticipant);
        $result->execute(array($idMatch));
        return $result->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getSportMatchRecherche($idMatch){
        $requete="select *  from matchs natural join sport where idMatch=? LIMIT 1";
        $result=self::$connexion->prepare($requete);
        $result->execute(array($idMatch));
        return $result->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getMatchesRecherche($r){
        $requete="select  * from matchs NATURAL join participe natural join equipe where gagnant is not NULL and  nomMatch like ? or nomTeam like ?  group by idmatch";
        $result = self::$connexion->prepare($requete);
        $result->execute(array('%'.$r.'%','%'.$r.'%'));
        return $result->fetchall(PDO::FETCH_ASSOC);
    }

    public function getidMatchesRecherche($r){
        $requete="select idMatch from matchs where nomMatch like ? and gagnant is not null";
        $result = self::$connexion->prepare($requete);
        $result->execute(array('%'.$r.'%'));
        return $result->fetchall(PDO::FETCH_ASSOC);
    }

    public function getMatchesSport($idSport){
        $requete="select * from matchs where idSport=? and gagnant is not null";
        $result = self::$connexion->prepare($requete);
        $result->execute(array($idSport));
        return $result->fetchall(PDO::FETCH_ASSOC);
    }

}