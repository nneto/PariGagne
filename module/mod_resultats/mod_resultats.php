<?php

/**
 * Created by PhpStorm.
 * User: max
 * Date: 21/11/2016
 * Time: 00:54
 */
include_once('controleur_resultats.php');
class ModResultats extends ModuleGenerique
{

    public function module_resultats (){
        $this->controleur=new ControleuResultats();
        $this->controleur->resultats();
    }

    public function module_resultats_recherche($recherche){
        $r= htmlspecialchars($recherche);
        $recherche = preg_replace('/\s{2,}/', ' ', trim($r));
        $this->controleur=new ControleuResultats();
        $this->controleur-> listeRechercheResultat($recherche);

    }

    public function module_details_resultats($idMatch){
        $this->controleur=new ControleuResultats();
        $this->controleur->detailsResultat($idMatch);
    }

    public function module_matches_sport($idSport){
        $this->controleur=new ControleuResultats();
        $this->controleur->listeSportMatch($idSport);
    }
}