<?php

/**
 * Created by PhpStorm.
 * User: max
 * Date: 21/11/2016
 * Time: 00:56
 */
class VueResultats extends VueGenerique
{
    public function afficheResultats($matches,$participe,$idMatches,$sports,$sportMatches){


        $cpt=0;
        $this->titre="resultats";
        $this->Css=array("<link href=\"module/mod_Matches/matchs.css\" rel=\"stylesheet\" type=\"text/css\">");
        $this->contenu.="	<article>
			<div class=\"row\" id=\"main\">
				<h2  id=\"titre\"> Matchs en cours </h2>
				<hr/>
					<div class=\"col-md-2\" >
						<div class=\"list-group\">
							<a href=\"#\" class=\"list-group-item disabled\">
					    		    Catégories 
					  		</a>
						";
        foreach ($sports as $sport){
            $nomSport=htmlspecialchars($sport['nomSport']);
            $this->contenu.= " <a href='index.php?module=resultats&idsport=$sport[idSport]' class='list-group-item'>$nomSport</a>";
        }
        $this->contenu.= "</div>
					</div>
					<div   class=\"col-md-6\">";
        foreach ($sportMatches as $sportMatch){
            $nomSport=htmlspecialchars($sportMatch[0]['nomSport']);
            $this->contenu.= " 
						<div class=\"panel panel-default\">
						  	<div class=\"panel-heading\">
						    		<h3 class=\"panel-title\">
						    		 $nomSport
						    		</h3>	
						    </div>";
            foreach($matches as $match){
                foreach ($idMatches as $idMatch){
                    if($match['idmatch'] == $idMatch['idmatch'] && $match['idSport'] == $sportMatch[0]['idSport']){
                        $nomMatch=htmlspecialchars($match['nomMatch']);
                        $this->contenu .= "<div class=\"panel-body list-group - item - text\">
						    		<a href='index.php?module=matchs&idmatche=$match[idmatch]'><h4 class=\"list-group-item-heading\">$nomMatch</h4></a>
						    		  <p >$match[dateMatch] $match[heureMatch]</p>";
                        foreach ($participe as $particpant){
                            if($idMatch['idmatch']==$particpant['idmatch'] && $cpt<=5 ){
                                $nomTeam=htmlspecialchars($particpant['nomTeam']);
                                $this->contenu .= "	
									<span class=\"label label-default\">$nomTeam: $particpant[score]</span>
						  		";
                                $cpt++;
                            }
                        }$cpt=0;
                        $this->contenu.="</div>";
                    }
                }
            }
            $this->contenu.="</div>";
        }
        $this->contenu.= "
					</div>
					<div class=\"col-md-4\">
					<div class=\"row\">
								<h4 class=\"pull-right\">Rechercher un resultat</h4>
								<br>
								<hr/>
								<form method='post' action='index.php?module=resultats' class=\"navbar-form navbar-right inline-form  row\">
					      				<div class=\"form-group\">
										<input name='recherche' type=\"search\" class=\"input-sm form-control\" placeholder=\"Recherche\">
										<button type=\"submit\" class=\"btn btn-primary btn-sm\"><span class=\"glyphicon glyphicon-eye-open\"></span> Chercher</button>
					     				</div>
					    			</form>
						</div>";
        if(!(isset($_SESSION)&&isset($_SESSION['idcompte'])&&isset($_SESSION['login'])&&isset($_SESSION['solde']))){
            $this->contenu.= " <div class=\"aside\">
						<div class=\"row\" >
							<h4 class=\"pull-right\">Si vous etes nouveau</h4>
							<br>
							<hr/>
							<p class=\"pull-right\">Ne tardez pas,venez vous inscrire et bénéficier de 50Euros gratuit.</p>
						        <p class=\"pull-right\"><a class=\"btn btn-primary\" href=\"#\" role=\"button\">Inscrivez vous</a></p>

						</div>
						</div>
						<br>";
        }
        $this->contenu.= "	<div class=\"row\" class=\"aside\">
							<h4 class=\"pull-right\">Toutes les infos pour mieux parier</h4>
							<br>
							<hr/>
							<div class=\"row\">
						        <p class=\"pull-right\"><a class=\"btn btn-default\" href=\"#\" role=\"button\">Statistiques</a></p>
							</div>
							<div class=\"row\">
							<p class=\"pull-right\"><a class=\"btn btn-default\" href=\"#\" role=\"button\">En savoir plus sur les paris</a></p>
							</div>
						</div>
					</div>
		</article>
		 ";
    }
    public function detailsResultat($detailMatch,$participants){
        $this->Css=array("<link href='module/mod_Matches/details-matchs.css' rel='stylesheet'type='text/css'> ");
        $d=date("d-m-Y", strtotime($detailMatch['dateMatch']));
        $this->titre.=$detailMatch['nomMatch'];
        $nomMatch=htmlspecialchars($detailMatch['nomMatch']);
        $this->contenu.="        
			<div class=\"row \" id=\"main\">
				<h2  id=\"titre\"> $nomMatch </h2>
				<hr/>
					<div class=\"col-md-8\" id='centre'>
						<div class=\" row text-center\">
						    <h3>$d</h3> <h3>$detailMatch[heureMatch]</h3>
						</div>
						<br>
						<div class=\"row\" >
							<div class=\"panel panel-default\">
						  		<div class=\"panel-heading\">
						    			<h3 class=\"panel-title\">Résultat du match</h3>
						  		</div>
						 		<div class=\"panel-body\">
						   		";
        foreach ($participants as $participant){
            if($participant['nomTeam']=='NUL'){
            }else{
                $nomTeam=htmlspecialchars($participant['nomTeam']);
                $this->contenu.=" <p class=\"equipe\" ><img class='logo' alt=''src='source/$participant[logo]' > $nomTeam<span class=\"badge\">$participant[score]</span> </p>";
            }
        }
        $this->contenu.= "
                    </div>
                     </div>";
        $this->contenu.="             </div>
					</div>
					<div class=\"col-md-4\" id='droite'>
					<div class=\"row\">
								<h4 class=\"pull-right\">Rechercher un match</h4>
								<br>
								<hr/>
								<form method='post' action='index.php?module=recherche' class=\"navbar-form navbar-right inline-form  row\">
					      				<div class=\"form-group\">
										<input type=\"search\" class=\"input-sm form-control\" placeholder=\"Recherche\">
										<button type=\"submit\" class=\"btn btn-primary btn-sm\"><span class=\"glyphicon glyphicon-eye-open\"></span> Chercher</button>
					     				</div>
					    			</form>
						</div>";
        if(! isset($_SESSION)&&isset($_SESSION['idcompte'])&&isset($_SESSION['login'])&&isset($_SESSION['solde']) && isset($_SESSION['admin'])) {
            if($_SESSION['admin']==0)
                $this->contenu.= " <div class=\"aside\">
						<div class=\"row\" >
							<h4 class=\"pull-right\">Si vous etes nouveau</h4>
							<br>
							<hr/>
							<p class=\"pull-right\">Ne tardez pas,venez vous inscrire et bénéficier de 50Euros gratuit.</p>
						        <p class=\"pull-right\"><a class=\"btn btn-primary\" href=\"#\" role=\"button\">Inscrivez vous</a></p>

						</div>
						</div>
						<br>";
        }
        $this->contenu.= "	<div class=\"row\" class=\"aside\">
							<h4 class=\"pull-right\">Toutes les infos pour mieux parier</h4>
							<br>
							<hr/>
							<div class=\"row\">
						        <p class=\"pull-right\"><a class=\"btn btn-default\" href=\"#\" role=\"button\">Statistiques</a></p>
							</div>
							<div class=\"row\">
							<p class=\"pull-right\"><a class=\"btn btn-default\" href=\"#\" role=\"button\">En savoir plus sur les paris</a></p>
							</div>
						</div>
					</div>
		</article>";
    }
}