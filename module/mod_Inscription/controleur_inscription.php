<?php

/**
 * Created by PhpStorm.
 * User: max
 * Date: 02/11/2016
 * Time: 21:02
 */
require_once('model_inscription.php');
require_once('vue_inscription.php');
class ControleurInscription extends ControleurGenerique
{

    /**
     * ControleurInscription constructor.
     */
    public function __construct()
    {
        $this->vue=new VueInscription();
        $this->modele=new ModelInsctription();
    }

    public function controleur_initForm(){
        $token=$this->modele->createToken();
        $this->vue->vue_afficheForm($token);
    }

    public function controleur_inscription($tab)
    {
        $r=$this->modele->getToken($tab['token']);
        if(empty($r)){
            $this->modele->effacerToken($tab['token']);
            $this->vue->vue_afficheFormErreur("erreur inscription");
        }else{
            $this->modele->effacerToken($tab['token']);
            $correct = $this->modele->addPersonne($tab);
            if ($correct == "email fail") {
                $this->vue->vue_afficheFormErreur("email déjà utiliser");
            } else if ($correct == "pseudo fail") {
                $this->vue->vue_afficheFormErreur("pseudo déjà utiliser");
            }else if($correct=="mdp different"){
                $this->vue->vue_afficheFormErreur("mdp different");
            }else{
                $this->vue->  vue_afficheInscription();
            }
        }
    }
}