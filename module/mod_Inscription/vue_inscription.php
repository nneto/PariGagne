<?php

/**
 * Created by PhpStorm.
 * User: max
 * Date: 31/10/2016
 * Time: 13:22
 */
class VueInscription extends VueGenerique
{
    public function __construct(){
        parent::__construct();
    }

    public function vue_afficheForm($token){
        $this->titre="Inscription";
        $this->contenu= "
    <article lass='col-md-5 col-md-offset-3'>
    <form   method=\"POST\"  action=\"index.php?module=inscription\" onsubmit=\"return verifAll() \" role=\"form\">
	    <input type='hidden' name='token' value='$token'>
	<h1>Inscrivez-vous</h1>
    <div class=\"formu\">
        <label class=\"formu1\" for=\"nom\">Nom :</label>
        <input placeholder=\"  Nom\" type=\"text\" maxlength=\"25\" name=\"nom\" id=\"nom\">
        <span    id=\"nomTips\">Doit être compris entre 3 et 100 caractère</span>
    </div>

    <div class=\"formu\">
        <label class=\"formu1\" for=\"prenom\">prenom :</label>
        <input placeholder=\" Prenom\" name=\"prenom\" type=\"text\" id=\"prenom\" />
         <span id=\"prenomTips\" >Doit être compris entre 3 et 100 caractère</span>
    </div>

    <div class=\"formu\">
        <label class=\"formu1\" for=\"message\">pseudo :</label>
        <input name=\"pseudo\"  type=\"text\" id=\"pseudo\" placeholder=\"  Pseudo\"/>
         <span id=\"pseudoTips\" >Doit être compris entre 5 et 30 caracrère</span>
    </div>

    <div class=\"formu\">
        <label class=\"formu1\" for=\"message\">Date de Naissance :</label>
        
        <script id=\"script\"  >
					$(function() {
						$( \"#date\" ).datepicker({
							showMonthAfterYear: false,
							inline : true,
							dateFormat: 'dd/mm/yy',
							changeMonth:true,
							changeYear:true,
							maxDate:'-18 y',
							yearRange: '1900:2013'
						});
					});
						</script>
        <input readonly placeholder='14/06/1998' type=\"text\" name=\"date\" id=\"date\" min=\"\" max=\"2016-06-25\"/>
         <span id='txtdateErreur'>Vous devez avoir au moins 18 ans pour s'inscrire</span>
    </div>

    <div class=\"formu\">
        <label class=\"formu1\" for=\"message\">Email :</label>
       <input placeholder=\"  exemple@gmail.com\" type=\"text\" name=\"email\" id=\"email\" maxlength=\"25\" lenght=\"40\">
   
    </div>

    <div class=\"formu\">
        <label class=\"formu1\" for=\"message\">Mot de passe :</label>
        <input placeholder=\"  Mot de passe\" type='password' name='mdp' id=\"mdp\" maxlength='25' lenght='40'>
         <span id=\"mdp1Tips\">Doit être compris entre 7 et 30 caractères  </span>
    </div>

    <div class=\"formu\">
        <label class=\"formu1\" for=\"message\">Confirmer votre mot de passe :</label>
	<input placeholder=\"  Confirmer votre mot de passe\" type='password' name='mdp2' id=\"mdp2\" maxlength='30' lenght='40'>
	<span id=\"mdp2Tips\">Le mot de passe doit être identique au mot de passe </span>
    </div>
	<button type=\"submit\" id=\"submit\" name=\"soumettre\" value=\"inscription\">Inscription</button>
	
</form>

			<script src='module/mod_Inscription/scriptInscription.js' type='text/javascript'></script>
    </article>
     </article>
";
        $this->Css=array(
            "<link rel='stylesheet' href='//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css'>",
            "<link href='module/mod_Inscription/PageInscription.css' rel='stylesheet' type='text/css'>",
	"<script src='https://code.jquery.com/jquery-1.12.4.js'></script>",
	"<link href='module/mod_Inscription/css/datepicker.css' rel='stylesheet' type='text/css'/>",
	"<script src='https://code.jquery.com/ui/1.12.1/jquery-ui.js'></script>")
	;
    }

    public function vue_afficheFormErreur($message){
        $this->titre="Inscription";
        $this->contenu= "
    <form method=\"POST\"  action=\"index.php?module=inscription\" onsubmit=\"return verifAll() \" role=\"form\">
	<h1>Inscrivez-vous</h1>
	<h2 style=color:#AA0000>$message</h2>
    <div class=\"formu\">
        <label class=\"formu1\" for=\"nom\">Nom :</label>
        <input placeholder=\"  Nom\" type=\"text\" maxlength=\"25\" name=\"nom\" id=\"nom\">
        <span    id=\"nomTips\">Doit être compris entre 3 et 100 caractère</span>
    </div>

    <div class=\"formu\">
        <label class=\"formu1\" for=\"prenom\">prenom :</label>
        <input placeholder=\" Prenom\" name=\"prenom\" type=\"text\" id=\"prenom\" />
         <span id=\"prenomTips\" >Doit être compris entre 3 et 100 caractère</span>
    </div>

    <div class=\"formu\">
        <label class=\"formu1\" for=\"message\">pseudo :</label>
        <input name=\"pseudo\"  type=\"text\" id=\"pseudo\" placeholder=\"  Pseudo\"/>
         <span id=\"pseudoTips\" >Doit être compris entre 5 et 30 caracrère</span>
    </div>

    <div class=\"formu\">
        <label class=\"formu1\" for=\"message\">Date de Naissance :</label>
        <br\>
        <script id=\"script\"  >
					$(function() {
						$( \"#date\" ).datepicker({
							showMonthAfterYear: false,
							inline : true,
							dateFormat: 'dd/mm/yy',
							changeMonth:true,
							changeYear:true,
							maxDate:'-18 y',
							yearRange: '1900:2013'
						});
					});
						</script>
        <input readonly placeholder='14/06/1998' type=\"text\" name=\"date\" id=\"date\" min=\"\" max=\"2016-06-25\"/>
         <span  >Vous devez avoir au moins 18 ans pour s'inscrire</span>
    </div>

    <div class=\"formu\">
        <label class=\"formu1\" for=\"message\">Email :</label>
       <input placeholder=\"  exemple@gmail.com\" type=\"text\" name=\"email\" id=\"email\" maxlength=\"25\" lenght=\"40\">
   
    </div>

    <div class=\"formu\">
        <label class=\"formu1\" for=\"message\">Mot de passe :</label>
        <input placeholder=\"  Mot de passe\" type='password' name='mdp' id=\"mdp\" maxlength='25' lenght='40'>
         <span id=\"mdp1Tips\">Doit être compris entre 7 et 30 caractères  </span>
    </div>

    <div class=\"formu\">
        <label class=\"formu1\" for=\"message\">Confirmer votre mot de passe :</label>
	<input placeholder=\"  Confirmer votre mot de passe\" type='password' name='mdp2' id=\"mdp2\" maxlength='30' lenght='40'>
	<span id=\"mdp2Tips\">Le mot de passe doit être identique au mot de passe </span>
    </div>
	<button type=\"submit\" id=\"submit\" name=\"soumettre\" value=\"inscription\">Inscription</button>
	
</form>
			<script src='module/mod_Inscription/scriptInscription.js' type='text/javascript'></script>
";
        $this->Css=array(
            "<link rel='stylesheet' href='//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css'>",
            "<link href='module/mod_Inscription/PageInscription.css' rel='stylesheet' type='text/css'>",
            "<script src='https://code.jquery.com/jquery-1.12.4.js'></script>",
            "<link href='module/mod_Inscription/css/datepicker.css' rel='stylesheet' type='text/css'/>",
            "<script src='https://code.jquery.com/ui/1.12.1/jquery-ui.js'></script>")
        ;
    }

    public function vue_afficheInscription()
    {
        $this->titre="Inscrit";
        $this->contenu="Inscription reussi";
    }
}