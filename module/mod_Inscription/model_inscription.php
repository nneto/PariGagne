<?php

/**
 * Created by PhpStorm.
 * User: max
 * Date: 02/11/2016
 * Time: 21:07
 */
//require_once('modele_inscription_exception.php');
class ModelInsctription extends ModeleGenerique
{


    public function addPersonne($tab){

        $requetePersonne="INSERT INTO personne VALUES (DEFAULT ,?,?,?,NULL)";
        $nom=htmlspecialchars($tab['nom']);
        $prenom=htmlspecialchars($tab['prenom']);
        $d=htmlspecialchars($tab['date']);
        $date = new DateTime($d);
        $dateDeNaissance=  $date->format('Y-m-d');

        $pseudo=htmlspecialchars($tab['pseudo']);
        $email=htmlspecialchars($tab['email']);
        $mdp=$this->mdpCrypt(htmlspecialchars($tab['mdp']),$pseudo);
        $mdp2=$this->mdpCrypt(htmlspecialchars($tab['mdp2']),$pseudo);
        $checkEmail=$this->checkEmail($email);
        $checkPseudo=$this->checkPseudo($pseudo);


        if($checkEmail) {
            return "email fail";
        }
        if($checkPseudo){
                return "pseudo fail";
        }

        $requete=self::$connexion->prepare($requetePersonne);
        $result=$requete->execute(array($nom,$prenom,$dateDeNaissance));
        $idPers=$this->getLastPersonne();
        $this->addCompte($pseudo,$email,$mdp,$idPers);
        $idCompte=$this->getLastCompte();
        $this->addComptePersonne($idCompte,$idPers);
    }

    public function addComptePersonne($idCompte,$idPers){
        $requeteCompte="update personne  set idCompte=? where idPers=? ";
        $requete=self::$connexion->prepare($requeteCompte);
        $requete->execute(array($idCompte,$idPers));
    }

    public  function addCompte($pseudo,$email,$mdp,$idPers){
        $requeteCompte="INSERT INTO compte(email, motDePasse, solde, pseudo, avatar, idPers)VALUES(?,?,DEFAULT,?,DEFAULT ,?)";
        $requete=self::$connexion->prepare($requeteCompte);
        $requete->execute(array($email,$mdp,$pseudo,$idPers));
    }

    public  function getLastPersonne(){
        $requeteIdPers="select * from personne ORDER BY idPers DESC limit 1";
        $result=self::$connexion->query($requeteIdPers);
        $t=$result->fetchall(PDO::FETCH_ASSOC);
        return $t[0]['idPers'];

    }

    public function getLastCompte(){
        $requeteCompte="select * from compte ORDER BY idCompte DESC limit 1";
        $result=self::$connexion->query($requeteCompte);
        $t=$result->fetchall(PDO::FETCH_ASSOC);
        return $t[0]['idcompte'];
    }

    public function checkEmail($email){
        $present=true;
        $requeteEmail='select email from compte where email=?';
        $requete=self::$connexion->prepare($requeteEmail);
        $requete->execute(array($email));
        $result=$requete->fetchall(PDO::FETCH_ASSOC);
        if(empty($result)){
            $present=false;
        }
        return $present;
    }

    public function checkPseudo($pseudo){
        $present=true;
        $requeteEmail='select pseudo from compte where pseudo=?';
        $requete=self::$connexion->prepare($requeteEmail);
        $requete->execute(array($pseudo));
        $result=$requete->fetchall(PDO::FETCH_ASSOC);
        if(empty($result)){
            $present=false;
        }
        return $present;
    }
}