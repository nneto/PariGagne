var nomCorrect=false;
var prenomCorrect=false;
var pseudoCorrect=false;
var motDePasseCorrect=false;

var inputNom=document.getElementById('nom');
var inputPrenom=document.getElementById('prenom');
var inputPseudo=document.getElementById('pseudo');
var inputMdp1=document.getElementById('mdp');
var inputMdp2=document.getElementById('mdp2');

window.onload = function(){
	document.getElementsByClassName('form-control').value = "";
}

/*Verifie que tout les champs sont bon*/
function verifAll(){
	if(nomCorrect && prenomCorrect && motDePasseCorrect && pseudoCorrect){
		return true;
	}else{
		return false;
	}
}

/*--------------------- Verification de nom-----------------*/
var tips = document.getElementById('nomTips');
inputNom.addEventListener("keyup",function(){
	var value = inputNom.value;
	if(value.length	<= 2){
		inputNom.style.borderColor="red";
		tips.style.visibility='visible';
		tips.innerHTML="Doit être compris entre 3 et 100 caractère";
		tips.style.color="red";
		nomCorrect=false;

	}else{
		inputNom.style.borderColor="green";
		tips.innerHTML="Saisie correct";
		tips.style.color="green";
		nomCorrect=true;
	}
	console.log(verifAll());
	verifAll();
});

/* ---------------------------Verification du prenom-------------------------*/
inputPrenom.addEventListener("keyup",function(){
	var tips = document.getElementById('prenomTips');
	var value = inputPrenom.value;

	if(value.length<= 2){
		inputPrenom.style.borderColor="red";
		tips.style.visibility='visible';
		tips.innerHTML="Doit être compris entre 3 et 100 caractère";
		tips.style.color="red";
		prenomCorrect=false;
	}else{
		inputPrenom.style.borderColor="green";
		tips.innerHTML="Saisie correct";
		tips.style.color="green";
		prenomCorrect=true;
	}
	console.log(verifAll());
	verifAll();
});

/*-----------------Verfication pseudo------------------------------*/

inputPseudo.addEventListener("keyup",function(){
		var tips=document.getElementById('pseudoTips');
		var value = inputPseudo.value;
		if((value.length<5) || (value.length>20)){
			inputPseudo.style.borderColor="red";
			tips.style.visibility='visible';
			tips.innerHTML="Doit être compris entre 5 et 20 caractères";
			tips.style.color="red";
			pseudoCorrect=false;
		}else{
			inputPseudo.style.borderColor="green";
			tips.innerHTML="Saisie correct";
			tips.style.color="green";
			pseudoCorrect=true;
		}
		console.log(verifAll());
		verifAll();
	}
);

/*-------VerificationMdp-------*/
inputMdp1.addEventListener("keyup",function(){
	var tips=document.getElementById('mdp1Tips');
	var tips2=document.getElementById('mdp2Tips');
	var value = inputMdp1.value;

	if(value.length <7){
		inputMdp1.style.borderColor="red";
		tips.style.visibility='visible';
		tips.innerHTML="Doit être compris entre 7 et 30 caractère";
		motDePasseCorrect=false;
	}else{
		tips.style.visibility='hidden';
		inputMdp1.style.borderColor="green";
		tips.style.color="green";
		tips.innerHTML="Saisie correct";
		if(inputMdp1.value==inputMdp2.value){

			tips.style.color="green";
			inputMdp2.style.borderColor="green";
			tips.innerHTML="Saisie correct"

			tips2.style.color="green";
			inputMdp1.style.borderColor="green";
			tips2.innerHTML="Saisie correct"
			motDePasseCorrect=true;
		}else{
			tips.style.visibility='visible';
			tips.style.color="red";
			tips2.style.visibility='visible';
			tips2.style.color="red";
			inputMdp2.style.borderColor="red";
			inputMdp1.style.borderColor="red";
			tips.innerHTML="Les deux mot de passe doit être identique";
			tips2.innerHTML="Les deux mot de passe doit être identique";
			motDePasseCorrect=false;
		}
	}
	console.log(verifAll());
	verifAll();
});

inputMdp2.addEventListener("keyup",function(){
	var tips=document.getElementById('mdp2Tips');
	var tips2=document.getElementById('mdp1Tips');
	var value = inputMdp2.value;

	if(value.length <7){
		inputMdp2.style.borderColor="red";
		tips.style.visibility='visible';
		tips.innerHTML="Doit être compris entre 7 et 30 caractère";
		motDePasseCorrect=false;
	}else{
		if(inputMdp1.value==inputMdp2.value){

			tips.style.color="green";
			inputMdp2.style.borderColor="green";
			tips.innerHTML="Saisie correct"

			tips2.style.color="green";
			inputMdp1.style.borderColor="green";
			tips2.innerHTML="Saisie correct"
			motDePasseCorrect=true;
		}else{
			tips.style.visibility='visible';
			tips.style.color="red";
			tips2.style.visibility='visible';
			tips2.style.color="red";
			inputMdp2.style.borderColor="red";
			inputMdp1.style.borderColor="red";
			tips.innerHTML="Les deux mot de passe doit être identique";
			tips2.innerHTML="Les deux mot de passe doit être identique";
			motDePasseCorrect=false;
		}
	}
	console.log(verifAll());
	verifAll();
});
	
	

	
