<?php

class ModeleProfil extends ModeleGenerique {

	
	public function get_profil($idCompte){
		$requete='select nom,prenom,dateDeNaissance,pseudo,solde,email,avatar from personne natural join compte where idCompte=?;';
		$result= self::$connexion->prepare($requete);
		$result -> execute(array($idCompte));
		return $result->fetchAll(PDO::FETCH_ASSOC);
	}

	public function desinscrire($idCompte){
        $requete='delete from compte where idCompte=?';
        $result= self::$connexion->prepare($requete);
        $result -> execute(array($idCompte));
        return $result->fetchAll(PDO::FETCH_ASSOC);
    }
	
	


}