<?php


include_once('controleur_profil.php');
class ModProfil extends ModuleGenerique {

    public function __construct() {
        $this-> controleur = new ControleurProfil();

    }

    public function module_profil($idCompte){
        $this->controleur->afficheProfil($idCompte);

    }

    public function module_desinscrire($idCompte){
        $this->controleur->desinscrire($idCompte);
    }
}