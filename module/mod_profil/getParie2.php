<?php
/**
 * Created by PhpStorm.
 * User: max
 * Date: 16/12/2016
 * Time: 15:42
 */

/**
 * Created by PhpStorm.
 * User: nneto
 * Date: 12/12/16
 * Time: 11:46
 */

include_once ('../../include/param_connexion.php');
$s="";
$idPast=0;
$connexion = new PDO($dns,$usr,$psw);
$connexion->exec("SET NAMES 'UTF8'");
$id=$_POST['id'];
$cpt=0;
$requete=" select * from parie natural join matchs natural join participe natural join equipe where gagne is not null and idCompte=$id";
$r=$connexion->query($requete);

$t= $r->FetchAll(PDO::FETCH_ASSOC);

$s.= "<div  style=\" height: 290px;\">
<table class='table table-bordered' style='font-weight:normal;'>
         <thead>
    <tr>
      <th>Nom du match</th>
      <th>Date</th>
      <th>heure</th>
      <th>Equipe</th>
      <th>mise</th>
      <th>cote</th>
      <th>Resultat</th>
    </tr>
  </thead>
   <tbody>
";

foreach ($t as $p){
    $nomMatch=htmlspecialchars($p['nomMatch']);
    $nomTeam=htmlspecialchars($p['nomTeam']);
    $gagne="perdue";
    if($p['gagne']==1){
        $gagne="gagner";
    }
    $s.="
        <tr>    
            <td>$nomMatch</td>    
            <td>$p[dateMatch]</td>
            <td>$p[heureMatch]</td>
            <td>$nomTeam</td>
            <td>$p[mise]</td>
            <td>$p[cote]</td>
            <td>$gagne</td>
        ";
}
$s.=  "
</tbody> 
</table>
</div>";
echo $s;
