<?php

require_once('modele_profil.php');
require_once('vue_profil.php');
class ControleurProfil extends ControleurGenerique {

    public function __construct() {
        $this->vue=new VueProfil();
        $this->modele=new ModeleProfil();

    }

    public function afficheProfil($idCompte){
        $idCompte=htmlspecialchars($idCompte);
        $profil= $this->modele-> get_profil($idCompte);
        $this->vue->vue_afficheP($profil[0]);
    }

    public function desinscrire($idCompte){
        $idCompte=htmlspecialchars($idCompte);
        $this->modele->desinscrire($idCompte);
        session_destroy();
        header('Location:index.php?module=accueil');

    }

}