<?php

class VueProfil extends VueGenerique {


    public function vue_afficheP($profil){
        $this -> titre='profil';
        $this->Css=array(
            "<link href='profil.css' rel='stylesheet' type='text/css'>",
            "<script src='https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.bundle.js'></script>",
            "<script src='https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.bundle.min.js'></script>",
            "<script src='https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.js'></script>",
            "<script src='https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js'></script>",


        );
        $nom=htmlspecialchars($profil['nom']);
        $prenom=htmlspecialchars($profil['prenom']);
        $pseudo=htmlspecialchars($profil['pseudo']);
        $email=htmlspecialchars($profil['email']);
        $this -> contenu .="
			<article id='main'>
				<div class='container'>
    					<div class='row profile '>
						<div class='col-md-3 thumbnail'>
							<div class='profile-sidebar'>
			
								<div class='profile-userpic'>
									<img src='source/avatar/$profil[avatar]'  class='img-responsive' alt=''>
								</div>
				
								<div class='profile-usertitle'>
									<div class='profile-usertitle-name'>
										$nom $prenom
									</div>
								</div>												
								<div class='profile-userbuttons '>
								<button type='button' style='width: 50% ; font-size: 9px' onclick='location.href=\"index.php?module=modifMdp \"' class='btn btn-success btn-sm'>Changer mot de passe</button>
									<button type='button' onclick='confirm()' class='btn btn-danger btn-sm'>Désinscription</button>
								</div>					
								<div class='profile-usermenu'>
									<ul class='nav'>
										<li >
											<a href='index.php?module=modifPseudo'>
												<i class='glyphicon glyphicon-user'></i>
											$pseudo </a>											
										</li>
										<li>
											<a href='#'>
												<i class='glyphicon glyphicon-home'></i>
											$profil[dateDeNaissance] </a>
										</li>
										<li>
											<a href='index.php?module=modifEmail' target='_blank'>
												<i class='glyphicon glyphicon-envelope'></i>
											$email </a>
										</li>
										<li>
											<a href='#'>
												<i class='glyphicon glyphicon-euro'></i>
											$profil[solde] </a>
										</li>
									</ul>
								</div>
				
							</div>
						</div>
						<div class='col-md-9 '>
							<ul class='nav nav-pills'>
    							<li class='active'><a data-toggle='pill' href='#gains'>Mes Gains</a></li>
   					 		<li><a data-toggle='pill' href='#paris' id='b' onclick='affiche($_SESSION[idcompte])' >Parie en cours</a></li>
   					 		<li><a data-toggle='pill' href='#paris' id='b' onclick='affiche2($_SESSION[idcompte])' >Parie effectuer</a></li>
   					 	</ul>   					 	   					 	    								
            						<div class='profile-content thumbnail'>
            						<div class='tab-content'>
            							<div id='gains' class='tab-pane fade in active'>
            								<canvas id='myChart' width='400' height='200'></canvas>
            								<script src='module\mod_profil\statistique.js'></script>
										
								</div>
								<div id='paris' class='tab-pane fade'>
									<h3> Mes Paris </h3>
									<p id='a'>   </p>
								</div>
							</div>


															
			   						<!--	<div id='tabs' style='height: 290px'>
													
													<div id='tabs-1' style='height: 225px'>
														<div id='chartContainer1' style='height: 240px; width: 100%;'>
	
	
													});
														</div>
													</div>
												<div id='tabs-2' style='height: 225px'>
													<div id='chartContainer2' style='height: 240px; width: 100%;'></div>
												</div>
											</div>-->
            						</div>
						</div>
					</div>
				</div>
				<br>
				<br>
				 <script src='module/mod_profil/bootstrap.min.js'></script>
				<script src='module\mod_profil\desinscription.js'></script>
				<script src='module\mod_profil\profil.js'></script>
				<script src='module/mod_profil/bootbox.min.js'></script>
				
		</article>

	";
    }
}