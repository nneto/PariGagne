<?php

/**
 * Created by PhpStorm.
 * User: nneto
 * Date: 09/11/16
 * Time: 18:42
 */
require_once('model_connexion.php');
require_once('vue_connexion.php');
class ControleurConnexion extends ControleurGenerique
{
    public function __construct()
    {
        $this->vue=new VueConnexion();
        $this->modele=new ModelConnexion();
    }

    public function controleur_initForm(){
        $this->vue->vue_afficheForm();
    }

    public function controleur_inscription($tab){
        $r=$this->modele->getToken($tab['token']);

        if(empty($r)){
            $this->modele->effacerToken($tab['token']);
            $this->vue->vue_erreur("erreur token");
        }else{
            $this->modele->effacerToken($tab['token']);
            $compte=$this->modele->getCompte($tab);
            if($compte==NULL){
                $this->vue->vue_erreur("identifiants incorrects");
            }else{
                $_SESSION['login']=$compte[0]['pseudo'];
                $_SESSION['idcompte']=$compte[0]['idcompte'];
                $_SESSION['solde']=$compte[0]['solde'];
                $_SESSION['admin']=$compte[0]['admin'];
            }
        }

    }
}